/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
var semaforoPeticion = true;
var isTablet = false;
function loadButtonsInit(){
	$('#datavolver').click(function(){
		consultarUsuarios('home');
		semaforoPeticion = true;
	});
	$('#infovolver').click(function(){
		consultarUsuarios('home');
		semaforoPeticion = true;
	});
	$('#btnAboutGraph').click(function(){
		pintarGrafica('');
		semaforoPeticion = true;
	});
	$('#home').click(function(){
		if(globalnif != ''){
			$('#ulusers').find('a.aDeleteBtn').each(function(){$(this).remove();});
			$('#'+globalnif).attr("onclick",globalOnClick);
		}
		semaforoPeticion = true;
	});
	$('#exportDDBB').click(function(){
		
		cordova.exec(function(result){
			semaforoPeticion = true;
		},errorPluginHandler, "ConsultaUser", 'exportdatabase',[]);
		semaforoPeticion = true;
	});
	
}


function validanif(dni){if(dni!=null&&dni!=''&&dni.length==9){numero=dni.substr(0,dni.length-1);let=dni.substr(dni.length-1,1);numero=numero%23;letra='TRWAGMYFPDXBNJZSQVHLCKET';letra=letra.substring(numero,numero+1);if(letra!=let.toUpperCase()){return false;}else{return true;}}
return false;}
function consultarUsuarios(page){navigator.notification.activityStart("Cargando...","Recuperando usuarios");cordova.exec(function(result){parseconsultarUsuarios(result,page);navigator.notification.activityStop();},errorPluginHandler,"ConsultaUser","allUsers",[]);}
function parseconsultarUsuarios(data,page){
	if(data!=null&&data.users!=null && data.users.length>0){
		if(navigator.connection.type == Connection.NONE){
			$('.parraInfo').show();
		}else{
			$('.parraInfo').hide();
		}
		var userstext='';
		var usuarios=data.users;
		var classz="border";
		for(i=0;i<usuarios.length;i++){
			userstext+='<li class="'+classz+'" id="'+usuarios[i].nif+'" onclick=\'hacerPeticion("'+usuarios[i].nif+'", "'+usuarios[i].bolsa+'")\'><strong>'+usuarios[i].name+'</strong> - ';userstext+='<span class="nifcss">'+usuarios[i].nif+'</span><br /><span class="bolsaText">'+usuarios[i].bolsatext+'</span>';if(usuarios[i].textofecha!=null){userstext+='&nbsp;-&nbsp;<span class="lastAct">Últ. act.:&nbsp;&nbsp;&nbsp;<span id="fecha">'+usuarios[i].textofecha+'</span> <span id="hora">'+usuarios[i].hora+'</span></span>'}
userstext+='</li>';classz="even";}
$('#imageContainer').hide();$('#ulusers').html(userstext);$('#ulusers').show();$('#ulusers').delegate('li','swiperight',function(){globalOnClick=$(this).attr("onclick");$('.aDeleteBtn').remove();globalnif=$(this).attr('id');var deleteBtn=$('<a id="eliminarButton">Eliminar</a>').addClass('aDeleteBtn');deleteBtn.addClass('ui-btn-up-r');$('#'+globalnif).attr("onclick","quitarDelete()");$(this).prepend(deleteBtn);$('#eliminarButton').click(function(e){e.stopPropagation();e.preventDefault();sendDelete(globalnif);return false;});});}else{$('#ulusers').hide();$('#ulusers').html('');$('.parra').show();$('#imageContainer').show();}
if(page!=null&&page!=''){$.mobile.changePage('#'+page);}
semaforoPeticion = true;}
function quitarDelete(){$('.aDeleteBtn').remove();$('#'+globalnif).attr("onclick",globalOnClick);}
var sendTodeleteVar=false;function sendDelete(id){globalnif=id;if(!sendTodeleteVar){sendTodeleteVar=true;navigator.notification.confirm('Estás seguro que quiere eliminar el usuario?',onConfirm,'Eliminar usuario','Cancelar,Eliminar');}
return false;}
function cargarInfoBolsa(result){if(result.yomismo!=null){var posglobdif="0";if(result.posicionglobaldif!=null){posglobdif=""+result.posicionglobaldif;if(posglobdif!=null&&posglobdif.indexOf("-")<0&&posglobdif!="0"){posglobdif="+"+posglobdif;}}
$('#dni_hidden').val(result.yomismo.nif);$('#posicionglobal').html('Ocupas el lugar nº '+result.posicionglobal+'<span class="difpos">&nbsp;&nbsp;&nbsp;&nbsp;('+posglobdif+')   </span>');$('#minombre').html('  '+result.yomismo.apellnom);$('#mipuntuacion').html(result.yomismo.notaopo);$('#miantiguedad').html(result.yomismo.tservi);if(result.primero!=null){$('#primerobolsa').show();$('#primeronombre').text('   '+result.primero.apellnom);$('#primeropuntu').html('  '+result.primero.notaopo);$('#primeroantiguedad').html('  '+result.primero.tservi);}else{$('#primerobolsa').hide();}
var provindata="";var auxTabla='<div class="ui-block-!LETRA"><div class="ui-bar-a">!CIUDAD</div><div class="datos"><strong>!POS</strong><span class="difpos">&nbsp;&nbsp;(!DIFPOS)</span></div></div>';var auxTablaVacio='<div class="ui-block-b"></div>';if(result.bolsas!=null){for(i=0;i<result.bolsas.length;i++){letraTabla='a';if(i%2!=0){letraTabla='c';}
var cambioPos=""+result.bolsas[i].change;if(cambioPos!=null&&cambioPos.indexOf("-")<0&&cambioPos!="0"){cambioPos="+"+cambioPos;}
provindata=provindata+auxTabla.replace('!LETRA',letraTabla).replace('!CIUDAD',result.bolsas[i].lugar).replace('!POS',result.bolsas[i].pos).replace('!DIFPOS',cambioPos);if(letraTabla=='a'){provindata=provindata+auxTablaVacio;}}}
$('#page1').addClass('ui-page ui-body-c ui-dialog ui-overlay-a');if(result.lastChangeDate!=null){$("#ultimocambiodate").text(result.lastChangeDate);}
$('#parseProvincias').html(provindata);navigator.notification.activityStop();$('#home').addClass('ui-page ui-body-c');$.mobile.changePage("#resultadoBolsa");$('#ulusers').undelegate('li','swiperight');$("#nif").val('');$("#optBolsa").val('-1').selectmenu('refresh');}else{navigator.notification.activityStop();navigator.notification.alert('No se han encontrado datos',null,"Información",'Cerrar');}
semaforoPeticion = true;}
var myScroll,pullDownEl,pullDownOffset,generatedCount=0;function loaded(){pullDownEl=document.getElementById('pullDown');pullDownOffset=pullDownEl.offsetHeight;myScroll=new iScroll('inicio',{useTransition:true,topOffset:pullDownOffset,onRefresh:function(){if(pullDownEl.className.match('loading')){pullDownEl.className='';pullDownEl.querySelector('.pullDownLabel').innerHTML='Desliza para refrescar...';}},onScrollMove:function(){if(this.y>5&&!pullDownEl.className.match('flip')){pullDownEl.className='flip';pullDownEl.querySelector('.pullDownLabel').innerHTML='Suelta para refrescar...';this.minScrollY=0;}else if(this.y<5&&pullDownEl.className.match('flip')){pullDownEl.className='';pullDownEl.querySelector('.pullDownLabel').innerHTML='Desliza para refrescar...';this.minScrollY=-pullDownOffset;}},onScrollEnd:function(){if(pullDownEl.className.match('flip')){pullDownEl.className='loading';pullDownEl.querySelector('.pullDownLabel').innerHTML='Cargando...';pullDownAction();}}});setTimeout(function(){document.getElementById('inicio').style.left='0';},800);}
function pullDownAction(){cordova.exec(function(result){parseconsultarUsuarios(result,'');myScroll.refresh();},errorPluginHandler,"ConsultaUser","allUsers",[]);}

function pintarGrafica(action){

	if(action == null || action == ''){
		action = 'lastmovementsuser';
		$('.lastmovementsuser').attr('checked', true);
	}
	$("#contentplot").empty();

	navigator.notification.activityStart("Cargando...","Recuperando histórico");
	cordova.exec(function(result){
			semaforoPeticion = true;
			parsegraph(result, action);
			$('#'+action).addClass('ui-btn-up-c-active ui-btn-active');
			navigator.notification.activityStop();
		},errorPluginHandler, "ConsultaUser", action,[$('#dni_hidden').val()]);
	
	}

function parsegraph(result, action){
if(result!=null){
var datos=result.positions;var sevilla=[];var almeria=[];var cadiz=[];var cordoba=[];var general=[];var granada=[];var huelva=[];var jaen=[];var malaga=[];
for(i=0;i<datos.length;i++){if(datos[i].almeria!='-1'){almeria.push([datos[i].fecha,parseFloat(datos[i].almeria)]);}
if(datos[i].cadiz!='-1'){cadiz.push([datos[i].fecha,parseFloat(datos[i].cadiz)]);}
if(datos[i].cordoba!='-1'){cordoba.push([datos[i].fecha,parseFloat(datos[i].cordoba)]);}
if(datos[i].granada!='-1'){granada.push([datos[i].fecha,parseFloat(datos[i].granada)]);}
if(datos[i].huelva!='-1'){huelva.push([datos[i].fecha,parseFloat(datos[i].huelva)]);}
if(datos[i].jaen!='-1'){jaen.push([datos[i].fecha,parseFloat(datos[i].jaen)]);}
if(datos[i].malaga!='-1'){malaga.push([datos[i].fecha,parseFloat(datos[i].malaga)]);}
if(datos[i].sevilla!='-1'){sevilla.push([datos[i].fecha,parseFloat(datos[i].sevilla)]);}
if(datos[i].general!='-1'){general.push([datos[i].fecha,parseFloat(datos[i].general)]);}}
if(datos!=null&&datos.length>0){$(function(){
	var data=[];
	var cont = 0;
	if(general != null && general.length > 0){
		data[cont] = {data:general,label:"General",points:{show:true},lines:{show:true}};
		cont++;
	}
	if(cadiz != null && cadiz.length > 0){
		data[cont] = {data: cadiz,label:"Cádiz",points:{show:true},lines:{show:true}};
		cont++;
	}
	if(cordoba != null && cordoba.length > 0){
		data[cont] = {data: cordoba,label:"Córdoba",points:{show:true},lines:{show:true}};
		cont++;
	}
	if(almeria != null && almeria.length > 0){
		data[cont] = {data: almeria,label:"Almería",points:{show:true},lines:{show:true}};
		cont++;
	}
	if(granada != null && granada.length > 0){
		data[cont] = {data: granada,label:"Granada",points:{show:true},lines:{show:true}};
		cont++;
	}
	if(huelva != null && huelva.length > 0){
		data[cont] = {data: huelva,label:"Huelva",points:{show:true},lines:{show:true}};
		cont++;
	}
	if(jaen != null && jaen.length > 0){
		data[cont] = {data: jaen,label:"Jaén",points:{show:true},lines:{show:true}};
		cont++;
	}
	if(malaga != null && malaga.length > 0){
		data[cont] = {data: malaga,label:"Málaga",points:{show:true},lines:{show:true}};
		cont++;
	}
	if(sevilla != null && sevilla.length > 0){
		data[cont] = {data: sevilla,label:"Sevilla",points:{show:true},lines:{show:true}};
		cont++;
	}
	
	var minTickSizeArray = [12,"day"];
	var ticksNumber = 15;
	if(action == null || action == '' || action == 'lastmovementsuser'){
		minTickSizeArray = [3,"day"];
	}else if(action == 'lasttwomovementsuser'){
		minTickSizeArray = [7,"day"];
	}else if(action == 'lastfourmovementsuser'){
		minTickSizeArray = [13,"day"];
	}else if(action == 'allmovementsuser'){
		minTickSizeArray = [1,"month"];
	}
	
	var heiAux = $("#contentplot").css("height");
	if(heiAux != null && heiAux!= '233px'){
		$("#contentplot").css("height", $(document).height() * (2/3) - 10);
	}
	$.plot($("#contentplot"),data,{xaxes:[{mode:'time',timeformat:"%d-%b",minTickSize: minTickSizeArray,ticks: ticksNumber}],yaxes:[{position:'left',ticks:8, tickDecimals:0},{position:'right',ticks:8, tickDecimals:0}],});});}else{$('.contentplot').html('<p>Se necesitan al menos 2 movimientos para mostrar datos</p>');}}
	$.mobile.changePage("#graficas");
	

}
function getParameterByName(name){name=name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");var regexS="[\\?&]"+name+"=([^&#]*)";var regex=new RegExp(regexS);var results=regex.exec(window.location.search);if(results==null){return"";}else{return decodeURIComponent(results[1].replace(/\+/g," "));}}
function validarversion(){
	$.ajaxSetup({
		'beforeSend':function(xhr){
			try{xhr.overrideMimeType('text/html; charset=UTF-8');
			}catch(e){}
		   },
		scriptCharset:"UTF-8"
	});
	$.ajax({
		url:'http://www.zulu4me.com/wp-inf/cedstatus.php',
		type:"POST",
		responseType:'text/plain;charset=UTF-8',
		data:{version:'206'},
		contentType:'application/x-www-form-urlencoded;charset=UTF-8',
		dataType:'json',
		success:function(data){
			if(data != null){
				lastVersion=data.lastVersion;
				if(lastVersion!='206'){
					if(data.updateMessage != null && data.updateMessage != ''){
						navigator.notification.confirm(data.updateMessage,onConfirmUpdate,"Actualización disponible",'Cerrar,Actualizar');
					}
				}else if(data.statusMesage!=''){
					navigator.notification.alert(
					data.statusMesage,null,"Estado de la aplicación",'Cerrar');
				}else if(data.other!=''){
					navigator.notification.alert(data.other,null,"Información",'Cerrar');
				}
			}
		}
	});
}

function onConfirmUpdate(buttonUpdate){
	
	if(buttonUpdate == 2){
		location.href='market://details?id=ctrlzapps.infobolsaced';
	}
}

function hacerPeticion(niflista, bolsalista, notification){
	$('.aDeleteBtn').remove();
	if(semaforoPeticion){
		semaforoPeticion = false;
	
		var NIF = niflista;
		if(NIF == null || NIF == ''){
   			NIF = $("#nif").val();
   		}else{
   			$("#"+NIF).addClass('userclicked');
   		}
   		var idBolsa = bolsalista;
   		if(idBolsa == null || idBolsa == ''){
			idBolsa = $("#optBolsa2 option:selected").val();
		}
		if(validanif(NIF)){
			if(idBolsa != null && idBolsa != '' && idBolsa != '-1'){
				var salvarDatos = "true";
				navigator.notification.activityStart("Cargando...","Recuperando información");
					cordova.exec(function (result){
						cargarInfoBolsa(result);
					}, errorPluginHandler, "Peticiones", "infoBolsa",[NIF, idBolsa, salvarDatos, notification]);
				}else{
					semaforoPeticion = true;
					navigator.notification.alert("Seleccione una bolsa",  null, "Aviso", "Cerrar");	
				}
		}else{
			semaforoPeticion = true;
			navigator.notification.alert("El DNI introducido no es válido",  null, "Aviso", "Cerrar");					
		}
		
	}
}
document.addEventListener("backbutton", handleBackButton, true);

function handleBackButton(){
	if($.mobile.activePage.attr('id') == 'home'){
		navigator.app.exitApp();
		
	}else if($.mobile.activePage.attr('id') == 'graficas'){
		$.mobile.changePage('#resultadoBolsa');
	}else{
		
		if(navigator.connection.type == Connection.NONE){
			$('.parraInfo').show();
		}else{
			$('.parraInfo').hide();
		}
		consultarUsuarios();
		$.mobile.changePage('#home');
	}
	semaforoPeticion = true;
}
function onConfirm(button) {
	if(button == 2){
		cordova.exec(function (result){
			parseconsultarUsuarios(result, '');
		}, errorPluginHandler, "ConsultaUser", "deleteUser",[globalnif]);
		globalOnClick = '';
		globalnif = '';
		sendTodeleteVar = false;
	}else{
		$('#ulusers').find('a.aDeleteBtn').each(function(){$(this).remove();});
		$('#'+globalnif).attr("onclick",globalOnClick);
		globalnif = '';
		globalOnClick = '';
		sendTodeleteVar = false;
	}
	semaforoPeticion = true;
}