/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps.alarm.manager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import ctrlzapps.CtrlzAppsUtils;
import ctrlzapps.beans.Bolsa;
import ctrlzapps.infobolsaced.InfobolsaConstants;
import ctrlzapps.infobolsaced.database.DataBase;
import ctrlzapps.plugins.PeticionesPlugin;
import ctrlzapps.plugins.statusBarNotification.StatusBarNotification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Clase que recibe la alarma
 * @author ctrlzapps
 *
 */
public class AlarmReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		
		new ComprobarAsync().execute(context);
	}

	/**
	 * Comprobacion de las posicioens al saltar la alarma
	 * @author ctrlzapps
	 *
	 */
	private class ComprobarAsync extends AsyncTask<Context, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Context... ctx) {
			DataBase db = new DataBase(ctx[0]);

			try {
				
				if(CtrlzAppsUtils.networkAvailable(ctx[0])){//Comprobamos que haya conexión a internet
					List<Bolsa> bolsas = db.getAllBolsas();
					PeticionesPlugin petPlugin = new PeticionesPlugin();
					List<Bolsa> bolsastToUpdate = new ArrayList<Bolsa>();
					Map<String, JSONObject> bolsasConsultadas = new HashMap<String, JSONObject>();
					for (Bolsa bolsa : bolsas) {
						JSONObject lastActBolsa = null;
						if (!bolsasConsultadas.containsKey(bolsa.getBolsa())) {
							lastActBolsa = petPlugin.getLastAct(bolsa.getBolsa());
							bolsasConsultadas.put(bolsa.getBolsa(), lastActBolsa);
						} else {
							lastActBolsa = bolsasConsultadas.get(bolsa.getBolsa());
						}
	
						String fecha = lastActBolsa.getString("textofecha") + " "
								+ lastActBolsa.getString("hora");
						SimpleDateFormat format = new SimpleDateFormat(
								InfobolsaConstants.LASTACTFORMAT);
						Date d = format.parse(fecha);
	
						if (d.after(bolsa.getLastAct())) {
							bolsa.setLastAct(d);
							bolsastToUpdate.add(bolsa);
							JSONObject datosReq = petPlugin.getBolsasData(
									bolsa.getNif(), bolsa.getBolsa(), "true", ctx[0]);
							Integer posGeneralDif = datosReq.getInt("posicionglobaldif");
							if (posGeneralDif != 0) {
								Log.d("AlarmReceiver", "Actualización Bolsa con cambios");
								StatusBarNotification barNot = new StatusBarNotification();
								String textDifPosActual = "" + posGeneralDif;
								if (posGeneralDif > 0) {
									textDifPosActual = "+" + posGeneralDif;
								}
								String posicionglobal = datosReq
										.getString("posicionglobal");
								String textoGeneral = "General -> "
										+ posicionglobal + " (" + textDifPosActual
										+ ")";
								String textNotifi = datosReq
										.getJSONObject("yomismo")
										.getString("apellnom").split(",")[1]
										+ " - " + textoGeneral;
								barNot.showNotification(ctx[0], datosReq
										.getJSONObject("yomismo").getString("nif"),
										"InfoBolsa CED", textNotifi);
							} else {
								Log.d("AlarmReceiver", "Actualización Bolsa sin cambios");
								
								/*StatusBarNotification barNot = new StatusBarNotification();
								barNot.showNotification(ctx[0], datosReq
										.getJSONObject("yomismo").getString("nif"),
										"No hay cambios en InfoBolsa CED",
										"No hay cambios, pero la bolsa se ha actualizado");*/
								
							}
						}else{
							Log.d("AlarmReceiver", "Sin actualización");
							/*StatusBarNotification barNot = new StatusBarNotification();
							barNot.showNotification(ctx[0], bolsa.getNif(),
									bolsa.getNif(),
									"No hay cambios, pero la bolsa se ha actualizado");*/
						}
					}
					if (bolsastToUpdate.size() > 0) {
						db.updateBolsasLastAct(bolsastToUpdate);
					}
				}else{
					Log.d("network", "No hay conexión disponible");
				}
			} catch (Exception e) {
				Log.e("receive", "Error procesando alarma "+e.getMessage());
			} finally {
				db.close();
			}
			return true;
		}
	}
}