/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps.alarm.manager;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;
/**
 * Control de la alarma que se ejecuta al iniciar el dispositivo
 * @author ctrlzapps
 *
 */
public class AlarmOnBootReceiver extends BroadcastReceiver {

  @Override
  public void onReceive(Context context, Intent intent) {
	  Log.d("AlarmManager", "startOnBoot: 900000ms");
	  Boolean notification = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("notification", Boolean.FALSE);
      if(notification){
    	  AlarmControl.startUpdateAlarm(context, 
			  Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("frecuenciaid", "900000")));
    	  Log.d("AlarmManager", "startOnBoot: 900000ms");
      }
  }
} 