/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps.alarm.manager;


import ctrlzapps.infobolsaced.InfobolsaConstants;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Control de Alarma
 * @author ctrlzapps
 *
 */
public class AlarmControl {

	
	public static void startUpdateAlarm(Context context, int intervalSeconds) {
		startUpdateAlarm(context, intervalSeconds, 0);
	}
	
   /**
    * Inicia o actualiza la alarma
    * @param context
    * @param intervalSeconds
    */
	public static void startUpdateAlarm(Context context, int intervalSeconds, int startDelay) {

		Intent intent = new Intent(context, AlarmReceiver.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, InfobolsaConstants.ALARM_ID, intent,
				PendingIntent.FLAG_NO_CREATE);
		if (pendingIntent == null) {

			pendingIntent = PendingIntent.getBroadcast(context, InfobolsaConstants.ALARM_ID, intent, 0);
			Log.d("AlarmManager", "PendingIntent " + InfobolsaConstants.ALARM_ID + " creado");
		}else{
			pendingIntent = PendingIntent.getBroadcast(context, InfobolsaConstants.ALARM_ID, intent,
					PendingIntent.FLAG_UPDATE_CURRENT);
			Log.d("AlarmManager", "PendingIntent " + InfobolsaConstants.ALARM_ID + " actualizado");
		}
		
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + startDelay, intervalSeconds,
				pendingIntent);
	}
	
	
	/**
	 * Cancela la alarma
	 * @param context
	*/
	public static void cancelAlarm(Context context) {

		Intent intent = new Intent(context, AlarmReceiver.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, InfobolsaConstants.ALARM_ID, intent,
				PendingIntent.FLAG_NO_CREATE);
		if (pendingIntent != null) {

			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.cancel(pendingIntent);
			Log.d("AlarmManager", "PendingIntent " + InfobolsaConstants.ALARM_ID + " cancelado");
		}
	}
} 
