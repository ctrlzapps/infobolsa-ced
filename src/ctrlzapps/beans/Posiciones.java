/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps.beans;

import java.util.Calendar;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.database.Cursor;
import android.util.Log;
/**
 * Bean de posiciones
 * @author ctrlzapps
 *
 */
public class Posiciones {

	public Integer id;
	public Integer general;
	public Integer almeria;
	public Integer cadiz; 
	public Integer cordoba; 
	public Integer granada;
	public Integer huelva;
	public Integer jaen;
	public Integer malaga;
	public Integer sevilla;
	public String nif;
	public Date fecha;
	
	public Posiciones(){}

	public Posiciones(Integer general, Integer almeria, 
			Integer cadiz, Integer cordoba, Integer granada, Integer huelva, 
			Integer jaen, Integer malaga, Integer sevilla, String nif, Date fecha){
		
		this.general = general;
		this.almeria = almeria;
		this.cadiz   = cadiz;
		this.cordoba = cordoba;
		this.granada = granada;
		this.huelva  = huelva;
		this.jaen    = jaen;
		this.malaga  = malaga;
		this.sevilla = sevilla;
		this.nif     = nif;
		this.fecha   = fecha;
	}
	
	public Posiciones(Cursor cp){
		
		if(cp.getColumnIndex("_id") > 0){
			this.id = cp.getInt(cp.getColumnIndex("_id"));
		}
		this.general = cp.getInt(cp.getColumnIndex("general"));
		this.nif = cp.getString(cp.getColumnIndex("nif"));
		Calendar gc= Calendar.getInstance();
		gc.setTimeInMillis(cp.getLong(cp.getColumnIndex("fecha")));
		this.fecha = gc.getTime();
		if(cp.getColumnIndex("almeria") > 0){
			this.almeria = cp.getInt(cp.getColumnIndex("almeria"));
		}else{
			this.almeria = -1;
		}
		if(cp.getColumnIndex("cadiz") > 0){
			this.cadiz = cp.getInt(cp.getColumnIndex("cadiz"));
		}else{
			this.cadiz = -1;
		}
		if(cp.getColumnIndex("cordoba") > 0){
			this.cordoba = cp.getInt(cp.getColumnIndex("cordoba"));
		}else{
			this.cordoba = -1;
		}
		if(cp.getColumnIndex("granada") > 0){
			this.granada = cp.getInt(cp.getColumnIndex("granada"));
		}else{
			this.granada = -1;
		}
		if(cp.getColumnIndex("huelva") > 0){
			this.huelva = cp.getInt(cp.getColumnIndex("huelva"));
		}else{
			this.huelva = -1;
		}
		if(cp.getColumnIndex("jaen") > 0){
			this.jaen = cp.getInt(cp.getColumnIndex("jaen"));
		}else{
			this.jaen = -1;
		}
		if(cp.getColumnIndex("malaga") > 0){
			this.malaga = cp.getInt(cp.getColumnIndex("malaga"));
		}else{
			this.malaga = -1;
		}
		if(cp.getColumnIndex("sevilla") > 0){
			this.sevilla = cp.getInt(cp.getColumnIndex("sevilla"));
		}else{
			this.sevilla = -1;
		}
	}
	
	/**
	 * Transforma a JSONObject
	 * @return
	 */
	public JSONObject toJson(){
		
		JSONObject posJson = new JSONObject();
		try {
			posJson.put("id", this.id);
			posJson.put("nif", this.nif);
			posJson.put("general", this.general);
			posJson.put("almeria", this.almeria);
			posJson.put("cadiz", this.cadiz);
			posJson.put("cordoba", this.cordoba);
			posJson.put("granada", this.granada);
			posJson.put("huelva", this.huelva);
			posJson.put("jaen", this.jaen);
			posJson.put("malaga", this.malaga);
			posJson.put("sevilla", this.sevilla);
			posJson.put("fecha", this.fecha.getTime());
		} catch (JSONException e) {
			Log.e("error", "Error parseando usuario  --> "+e.getMessage());
		}
		return posJson;
	}
	
	public Integer getGeneral() {
		return general;
	}
	
	public void setGeneral(Integer general) {
		this.general = general;
	}
	
	public Integer getAlmeria() {
		return almeria;
	}
	
	public void setAlmeria(Integer almeria) {
		this.almeria = almeria;
	}
	
	public Integer getCadiz() {
		return cadiz;
	}
	
	public void setCadiz(Integer cadiz) {
		this.cadiz = cadiz;
	}
	
	public Integer getCordoba() {
		return cordoba;
	}
	
	public void setCordoba(Integer cordoba) {
		this.cordoba = cordoba;
	}
	
	public Integer getGranada() {
		return granada;
	}
	
	public void setGranada(Integer granada) {
		this.granada = granada;
	}
	
	public Integer getHuelva() {
		return huelva;
	}
	
	public void setHuelva(Integer huelva) {
		this.huelva = huelva;
	}
	
	public Integer getJaen() {
		return jaen;
	}
	
	public void setJaen(Integer jaen) {
		this.jaen = jaen;
	}
	
	public Integer getMalaga() {
		return malaga;
	}
	public void setMalaga(Integer malaga) {
		this.malaga = malaga;
	}
	
	public Integer getSevilla() {
		return sevilla;
	}
	
	public void setSevilla(Integer sevilla) {
		this.sevilla = sevilla;
	}
	
	public String getNif() {
		return nif;
	}
	public void setNif(String nif) {
		this.nif = nif;
	}
	
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}
