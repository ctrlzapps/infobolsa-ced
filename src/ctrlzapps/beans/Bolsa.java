/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps.beans;

import java.util.Calendar;
import java.util.Date;

import android.database.Cursor;

/**
 * Bean para la gestión de las bolsas
 * @author ctrlzapps
 *
 */
public class Bolsa {

	/**
	 * Código de la bolsa
	 */
	private String bolsa;
	
	/**
	 * Nombre de la bolsa
	 */
	private String bolsaText;
	
	/**
	 * Fecha de última actualización
	 */
	private Date lastAct;

	/**
	 * Nif para la consulta
	 */
	private String nif;
	
	public Bolsa(String bolsa, String bolsaText, Date lastAct){
		this.bolsa = bolsa;
		this.bolsaText = bolsaText;
		this.lastAct = lastAct;
	}
	
	public Bolsa(Cursor cBolsa){
		
		this.bolsa = cBolsa.getString(cBolsa.getColumnIndex("bolsa"));
		this.bolsaText = cBolsa.getString(cBolsa.getColumnIndex("bolsatext"));
		if(cBolsa.getColumnIndex("lastAct") > 0){
			Calendar gc= Calendar.getInstance();
			gc.setTimeInMillis(cBolsa.getLong(cBolsa.getColumnIndex("lastAct")));
			this.lastAct = gc.getTime();
		}else{
			this.lastAct = new Date();
		}
		if(cBolsa.getColumnIndex("nif") > 0){
			this.nif = cBolsa.getString(cBolsa.getColumnIndex("nif"));
		}
	}
	
	public String getBolsa() {
		return bolsa;
	}

	public void setBolsa(String bolsa) {
		this.bolsa = bolsa;
	}

	public String getBolsaText() {
		return bolsaText;
	}

	public void setBolsaText(String bolsaText) {
		this.bolsaText = bolsaText;
	}

	public Date getLastAct() {
		return lastAct;
	}

	public void setLastAct(Date lastAct) {
		this.lastAct = lastAct;
	}
	
	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}
}
