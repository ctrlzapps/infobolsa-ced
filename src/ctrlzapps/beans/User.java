/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps.beans;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.database.Cursor;
import android.util.Log;

/**
 * Bean para guardar los datos de un usuario
 * @author ctrlzapps
 *
 */
public class User {

	private Integer id;
	private String name;
	private String nif;
	private String x;
	private String esp;
	private String cue;
	private String bolsa;
	private String bolsatext;
	private List<Posiciones> posiciones;
	
	public User(){}
	
	/**
	 * Constructor con parámetros
	 * @param id
	 * @param name
	 * @param nif
	 * @param x
	 * @param cue
	 * @param esp
	 * @param bolsa
	 */
	public User(String name, String nif, String x, String cue, String esp, String bolsa, String bolsatext){
		
		this.name = name;
		this.nif = nif;
		this.x = x;
		this.cue = cue;
		this.esp = esp;
		this.bolsa = bolsa;
		this.bolsatext= bolsatext;
	}
	
	/**
	 * Constructor por cursor
	 * @param c
	 */
	public User(Cursor c){
		
		if(c.getColumnIndex("_id") > 0){
			this.id = c.getInt(c.getColumnIndex("_id"));
		}
		this.name = c.getString(c.getColumnIndex("name"));
		this.nif = c.getString(c.getColumnIndex("nif"));
		if(c.getColumnIndex("x") > 0){
			this.x = c.getString(c.getColumnIndex("x"));
		}
		if(c.getColumnIndex("cue") > 0){
			this.cue = c.getString(c.getColumnIndex("cue"));
		}
		if(c.getColumnIndex("esp") > 0){
			this.esp = c.getString(c.getColumnIndex("esp"));
		}
		this.bolsa = c.getString(c.getColumnIndex("bolsa"));
		if(c.getColumnIndex("bolsatext") >0){
			this.bolsatext = c.getString(c.getColumnIndex("bolsatext"));
		}
		
	}

	public JSONObject toJson(){
		
		JSONObject userJson = new JSONObject();
		try {
			userJson.put("id", this.id);
			userJson.put("name", this.name);
			userJson.put("nif", this.nif);
			userJson.put("bolsa", this.bolsa);
			userJson.put("bolsatext", this.bolsatext);
			userJson.put("bolsatext", this.bolsatext);
		} catch (JSONException e) {
			Log.e("error", "Error parseando usuario  --> "+e.getMessage());
		}
		return userJson;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x = x;
	}

	public String getEsp() {
		return esp;
	}

	public void setEsp(String esp) {
		this.esp = esp;
	}

	public String getCue() {
		return cue;
	}

	public void setCue(String cue) {
		this.cue = cue;
	}

	public String getBolsa() {
		return bolsa;
	}

	public void setBolsa(String bolsa) {
		this.bolsa = bolsa;
	}

	public String getBolsatext() {
		return bolsatext;
	}

	public void setBolsatext(String bolsatext) {
		this.bolsatext = bolsatext;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Posiciones> getPosiciones() {
		return posiciones;
	}

	public void setPosiciones(List<Posiciones> posiciones) {
		this.posiciones = posiciones;
	}
}
