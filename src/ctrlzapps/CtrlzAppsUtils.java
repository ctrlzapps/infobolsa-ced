/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
/**
 * Clase de utilidades
 * @author ctrlzapps
 *
 */
public class CtrlzAppsUtils {

	/**
	 * Comprueba que exista una conexión
	 * @param context
	 * @return
	 */
	public static boolean networkAvailable(Context context) {
		
		ConnectivityManager connectMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectMgr != null) {
			NetworkInfo[] netInfo = connectMgr.getAllNetworkInfo();
			if (netInfo != null) {
				for (NetworkInfo net : netInfo) {
					if (net.getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		} else {
			Log.d("NETWORK", "Conexiones no disponibles");
		}
		return false;
	}
	
	/**
	 * Obtiene la posición en el array
	 * @param arrPrefs
	 * @param searchValue
	 * @return
	 */
	public static Integer getIndex(String[] arrPrefs, String searchValue){
		
		int indexSel = 2;	
		try{
			for(int i=0;i<arrPrefs.length;i++){
				if(arrPrefs[i].equals(searchValue)){
					indexSel = i;
					break;
				}
			}
		}catch(Exception e){
			indexSel = 2;
		}
		return indexSel;
		
	}
}
