/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps.infobolsaced;

import org.apache.cordova.DroidGap;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.analytics.tracking.android.EasyTracker;

import ctrlzapps.alarm.manager.AlarmControl;
import ctrlzapps.plugins.preferences.CtrlzAppsPreferenceActivity;

/**
 * Clase actividad principal
 * @author ctrlzapps
 *
 */
public class InfoBolsaCEDActivity extends DroidGap {
	// ADMOB - Declaración de variables
	private static final String MY_AD_UNIT_ID = "XXXXXXXXXXX";
	private AdView adView;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

    	super.onCreate(savedInstanceState);
    	super.init();

    	// Cargamos el mensaje de inicio
        super.setIntegerProperty("loadUrlTimeoutValue", 60000);
        // Cargamos la url principal
		Bundle extras = getIntent().getExtras();
		if (extras != null && extras.containsKey("notificationID") && !extras.containsKey("hecho")){
			super.setStringProperty("loadingDialog", super.getString(R.string.loadingData));
			Log.i(TAG, "Noti --> "+extras.get("notificationID").toString());
			String idNot = extras.get("notificationID").toString();
			super.loadUrl("file:///android_asset/www/index.html?nifnoti="+idNot);
		}else{
			try{
			    if(PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean("notification", Boolean.FALSE)){
			    	AlarmControl.startUpdateAlarm(getContext(),
						  Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(getContext()).getString("frecuenciaid", "900000")), 60000);
			    }
			}catch(Exception e){
				Log.e(TAG, "Error comprobando el servicio de notificaciones");
			}
			super.setStringProperty("loadingDialog", super.getString(R.string.initmessage));
			super.loadUrl("file:///android_asset/www/index.html");
		}

        //ADMOB - Creación de vista de anuncio
        adView = new AdView(this, AdSize.BANNER, MY_AD_UNIT_ID);
        LinearLayout layout = super.root;
        layout.addView(adView);
        // Para centrarlo en modo apaisado
        layout.setHorizontalGravity(android.view.Gravity.CENTER_HORIZONTAL);
        AdRequest adRequest = new AdRequest();
        //adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
        adView.loadAd(adRequest);
    }

	@Override
	public void onReceivedError(int errorCode, String description, String failingUrl) {
		try{
			if(!this.isFinishing()){
				super.loadUrl("file:///android_asset/www/index.html");
			}
		}catch(Exception e){
			Log.e(TAG,"Error en el Activity controlando el error");
		}
	}

    /**
     * Método de destrucción
     */
    @Override
    public void onDestroy() {
    	if (adView != null) {
    		adView.removeAllViews();
    		adView.destroy();
    	}
    	super.onDestroy();
    }

    /**
     * Arranca Google Analytics
     */
    @Override
    public void onStart() {
      super.onStart();
      EasyTracker.getInstance().activityStart(this);
    }

    /**
     * Para Google Analytics
     */
    @Override
    public void onStop(){
    	super.onStop();
    	EasyTracker.getInstance().activityStop(this);
    }

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);

    }

    /**
     * Cuando se pulsa en el botón del menú
     */
	public boolean onMenuItemSelected(int paramInt, MenuItem paramMenuItem){
		startActivity(new Intent(this, CtrlzAppsPreferenceActivity.class));
		return activityResultKeepRunning;
	}

	@Override
    public void onConfigurationChanged(Configuration nuevaconfig) {
		 super.onConfigurationChanged(nuevaconfig);
    }

}