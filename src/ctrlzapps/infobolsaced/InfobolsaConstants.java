/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps.infobolsaced;

/**
 * Constantes para la aplicación
 * @author ctrlzapps
 *
 */
public interface InfobolsaConstants {

	Integer ALARM_ID = 1047800;
	String DATABASE_NAME = "infobolsaced.db";
	Integer DATABASE_VERSION = 3;
	String DATEPARSE     = "dd/MM/yyyy HH:mm:ss";
	String DATEPARSE_DB  = "yyyy-MM-dd HH:mm:ss";
	String LASTACTFORMAT = "dd/MM/yyyy HH:mm";
	String DATE_PATTERN = "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)";
	String HOUR_PATTERN = "([01]?[0-9]|2[0-3]):[0-5][0-9]";
	String USER_TABLE      = "accounts";
	String POSITIONS_TABLE = "positions";
	String BOLSAS_TABLE = "bolsas";
	String CREATE_USER_SQL          = "CREATE TABLE accounts (_id INTEGER PRIMARY KEY AUTOINCREMENT, nif VARCHAR(10), name TEXT, x TEXT, cue TEXT, esp TEXT, bolsa TEXT, bolsatext TEXT)";
    String CREATE_POSITIONS_SQL     = "CREATE TABLE positions (_id INTEGER PRIMARY KEY AUTOINCREMENT, general INTEGER, almeria INTEGER, cadiz INTEGER, cordoba INTEGER, granada INTEGER, huelva INTEGER, jaen INTEGER, malaga INTEGER, sevilla INTEGER, nif VARCHAR(10), fecha DATETIME DEFAULT CURRENT_TIMESTAMP)";
    String CREATE_BOLSAS_SQL        = "CREATE TABLE bolsas (_id INTEGER PRIMARY KEY AUTOINCREMENT,  bolsa TEXT, bolsatext TEXT, lastAct DATETIME DEFAULT CURRENT_TIMESTAMP)";
    String ALTERTABLE_POSITIONS_SQL   = "DROP TABLE IF EXISTS positions;";
    String ALTERTABLE_BOLSAS_SQL      = "DROP TABLE IF EXISTS bolsas;";
	String ALTERTABLE_BOLSAS_DATA_SQL = "INSERT INTO bolsas (bolsa, bolsatext) SELECT DISTINCT bolsa TEXT, bolsatext FROM accounts;";
	String FROM_NOTIFICATIONS            = "notifications";
    String USER_ALL_SELECT_QUERY         = "SELECT _id, nif, name, bolsa, bolsatext FROM accounts ORDER BY name";
    String USER_SELECT_QUERY             = "SELECT _id, nif,name, bolsa,cue, x, esp, bolsatext FROM accounts where nif = ?";
    String POSICIONES_SELECT_QUERY       = "SELECT * FROM positions where nif = ? ORDER BY _id DESC LIMIT 1";
    String POSICIONES_SELECT_QUERY_2     = "SELECT * FROM positions where nif = ? ORDER BY _id DESC LIMIT 2";
    String POSICIONES_SELECT_QUERY_ALL   = "SELECT * FROM positions where nif = ? ORDER BY fecha DESC";
    String POSICIONES_SELECT_QUERY_DATE  = "SELECT * FROM positions where nif = ? AND fecha between  F1 AND F2 ORDER BY fecha DESC";
    String BOLSAS_SELECT_QUERY_ALL       = "SELECT DISTINCT bolsas.bolsa, bolsas.bolsatext, bolsas.lastAct, accounts.nif FROM bolsas, accounts WHERE bolsas.bolsa = accounts.bolsa";
    String DNIS_FROM_BOLSAS_SELECT_QUERY = "SELECT nif FROM accounts where bolsa = ? ORDER BY name";
    String SIMULATE_SECOND_SELECT_QUERY  = "SELECT accounts.nif, accounts.name, accounts.bolsatext, positions.general, positions.almeria, positions.cadiz, positions.cordoba, positions.granada, positions.huelva, positions.jaen, positions.malaga, positions.sevilla, positions.fecha  FROM accounts, positions where accounts.nif = ? AND accounts.nif = positions.nif  ORDER BY _id DESC LIMIT 2";
    
    String ANDALUCIA_ALMERIA = "almeria";
    String ANDALUCIA_CADIZ   = "cadiz";
    String ANDALUCIA_CORDOBA = "cordoba";
    String ANDALUCIA_GRANADA = "granada";
    String ANDALUCIA_HUELVA  = "huelva";
    String ANDALUCIA_JAEN    = "jaen";
    String ANDALUCIA_MALAGA  = "malaga";
    String ANDALUCIA_SEVILLA = "sevilla";
    
	/**
	 * Enumerado con con las url
	 * @author ctrlzapps
	 *
	 */
	enum UrlBolsa {
		BOLSAS {
		    public String toString() {
		        return "http://www.juntadeandalucia.es/educacion/vscripts/dgprh/BOLSAS/index.asp";
		    }
		},
		INDEX {
		    public String toString() {
		        return "http://www.juntadeandalucia.es/educacion/vscripts/dgprh/BOLSAS/[idBolsa]/index.asp";
		    }
		},
		 
		VER {
		    public String toString() {
		        return "http://www.juntadeandalucia.es/educacion/vscripts/dgprh/BOLSAS/[idBolsa]/Ver.asp";
		    }
		},
		 
		SELDNI {
		    public String toString() {
		        return "http://www.juntadeandalucia.es/educacion/vscripts/dgprh/BOLSAS/[idBolsa]/SelDNI.asp";
		    }
		}
	}
	
	int dot = 200;      // Length of a Morse Code "dot" in milliseconds
	int dash = 500;     // Length of a Morse Code "dash" in milliseconds
	int short_gap = 200;    // Length of Gap Between dots/dashes
	int medium_gap = 500;   // Length of Gap Between Letters
	int long_gap = 1000;    // Length of Gap Between Words
	long[] vibratePattern = {
	    0,  // Start immediately
	    dot, short_gap, dot, short_gap, dot,    // s
	    medium_gap,
	    dash, short_gap, dash, short_gap, dash, // o
	    medium_gap,
	    dot, short_gap, dot, short_gap, dot,    // s
	    long_gap
	};
}
