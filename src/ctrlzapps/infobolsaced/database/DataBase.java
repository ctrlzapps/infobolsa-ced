/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps.infobolsaced.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import ctrlzapps.beans.Bolsa;
import ctrlzapps.beans.Posiciones;
import ctrlzapps.beans.User;
import ctrlzapps.infobolsaced.InfobolsaConstants;
import ctrlzapps.plugins.PeticionesPlugin;
/**
 * Clase que se encarga de acceder ala BBDD
 * @author ctrlzapps
 *
 */
public class DataBase extends SQLiteOpenHelper{

	
	public DataBase(Context context) {
		super(context,InfobolsaConstants.DATABASE_NAME, null, InfobolsaConstants.DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(InfobolsaConstants.CREATE_USER_SQL);
		db.execSQL(InfobolsaConstants.CREATE_POSITIONS_SQL);
		db.execSQL(InfobolsaConstants.CREATE_BOLSAS_SQL);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if(oldVersion == 1){
			db.execSQL(InfobolsaConstants.ALTERTABLE_POSITIONS_SQL);
			db.execSQL(InfobolsaConstants.CREATE_POSITIONS_SQL);
		}
		if(newVersion == InfobolsaConstants.DATABASE_VERSION){
			db.execSQL(InfobolsaConstants.ALTERTABLE_BOLSAS_SQL);
			db.execSQL(InfobolsaConstants.CREATE_BOLSAS_SQL);
			db.execSQL(InfobolsaConstants.ALTERTABLE_BOLSAS_DATA_SQL);
		}
	}
	
	/**
	 * Obtiene a un usuario de la BBDD
	 * @param db
	 * @param nif
	 * @return
	 */
	public User getUserByNif(String nif){
		User us = null;
		SQLiteDatabase db = null;
		Cursor users = null;
		try{
			db = SQLiteDatabase.openOrCreateDatabase(getReadableDatabase().getPath(), null);
			users = db.rawQuery(InfobolsaConstants.USER_SELECT_QUERY, new String[]{nif});
			if(users.moveToFirst()){
				us = new User(users);
			}
			
		}catch(Exception e){
			Log.e("DATABASE", "getUserByNif: "+nif+ " - "+e.getMessage());
		}finally{
			if(users != null){
				users.close();
			}
			if(db != null){
				db.close();
			}
		}
		return us;
	}
	
	/**
	 * Guarda el usuario o lo consulta si existe
	 * @param nif
	 * @param cue
	 * @param esp
	 * @param x
	 * @param name
	 * @return
	 */
	public User saveData(String nif, String cue, String esp, String x, String name, String bolsa, String bolsatext){
		
		SQLiteDatabase db = null;
		User us = new User();
		try{
			ContentValues cv = new ContentValues();
			cv.put("nif", nif);
			cv.put("name", name);
			cv.put("x", x);
			cv.put("cue", cue);
			cv.put("esp", esp);
			cv.put("bolsa", bolsa);
			cv.put("bolsatext", bolsatext);
			db = SQLiteDatabase.openOrCreateDatabase(getWritableDatabase().getPath(), null) ;
			long rowid = db.insertOrThrow(InfobolsaConstants.USER_TABLE, null, cv);
			if(rowid > 0){
				us =  new User(name, nif, x, cue, esp, bolsa, bolsatext);
			}
		}catch(Exception e){
			Log.e("DATABASE", "saveData: "+e.getMessage());
		}finally{
			if(db != null){
				db.close();
			}
		}
		return us;
	}
	
	/**
	 * Obtiene todos los usuarios
	 * @return
	 */
	public JSONArray getAllUsers(){
		
		SQLiteDatabase db = null;
		JSONArray usersArray = new JSONArray();
		Cursor cUsers = null;
		try{
			db = SQLiteDatabase.openOrCreateDatabase(getReadableDatabase().getPath(), null);
			cUsers = db.rawQuery(InfobolsaConstants.USER_ALL_SELECT_QUERY, new String[]{});
			Map<String, JSONObject> bolsasConsultadas = new HashMap<String, JSONObject>();
			if(cUsers.moveToFirst()){
				do{
					User userSelect = new User(cUsers);
					JSONObject userj = userSelect.toJson();
					try{
						String bolsa = userj.get("bolsa").toString();
						if(!bolsasConsultadas.containsKey(bolsa)){
							JSONObject lastAc = new PeticionesPlugin().getLastAct(bolsa);
							userj.put("textofecha", lastAc.get("textofecha"));
							userj.put("hora", lastAc.get("hora"));
						}else{
							JSONObject lastAc = bolsasConsultadas.get(bolsa);
							userj.put("textofecha", lastAc.get("textofecha"));
							userj.put("hora", lastAc.get("hora"));
						}
					}catch(Exception e){
						Log.e("error", "Error obteniendo la última actualización de la bolsa "+e.getMessage());
					}
					usersArray.put(userj);
				}while(cUsers.moveToNext());
			}
			
		}catch(Exception e){
			Log.e("DATABASE", "getAllUsers: "+e.getMessage());
		}finally{
			if(cUsers != null){
				cUsers.close();
			}
			if(db != null){
				db.close();
			}
		}
		return usersArray;
	}

	/**
	 * Elimina el usuario pasado por el nif
	 * @param nif
	 * @return
	 */
	public boolean deleteUser(String nif){
		
		SQLiteDatabase db = null;
		int result = -1;
		try{
			db = SQLiteDatabase.openOrCreateDatabase(getReadableDatabase().getPath(), null);
			result = db.delete(InfobolsaConstants.USER_TABLE, "nif = ?", new String[]{nif});
			if(result > 0){
				result = db.delete(InfobolsaConstants.POSITIONS_TABLE, "nif = ?", new String[]{nif});
			}
		}catch(Exception e){
			Log.e("DATABASE", "deleteUser: "+nif + " - "+e.getMessage());
		}finally{
			if(db != null){
				db.close();
			}
		}
		return (result > 0);
	}
	
	/**
	 * Guarda una consulta de posiciones en la bbdd
	 * @param bolsas Objeto JSONArray con todas las posiciones
	 * @param global posicion general
	 * @param nif nif dela persona
	 * @return Devuelve las posiciones
	 */
	public Posiciones savePosiciones(JSONArray bolsas, String global, String nif){
		
		Integer general = Integer.valueOf(global);
		Integer almeria = -1;
		Integer cadiz = -1;
		Integer cordoba = -1;
		Integer granada = -1;
		Integer huelva = -1;
		Integer jaen = -1;
		Integer malaga = -1;
		Integer sevilla = -1;
		// sacamos los elementos del JSONArray
		for(int i=0;i<bolsas.length();i++){
			try {
				JSONObject bolsa = bolsas.getJSONObject(i);
				String lugar = bolsa.getString("lugar").toLowerCase();
				lugar = limpiarAcentos(lugar);
				if(lugar.equals(InfobolsaConstants.ANDALUCIA_ALMERIA)){
					almeria = Integer.valueOf(bolsa.getString("pos"));
				}else if(lugar.equals(InfobolsaConstants.ANDALUCIA_CADIZ)){
					cadiz = Integer.valueOf(bolsa.getString("pos"));
				}else if(lugar.equals(InfobolsaConstants.ANDALUCIA_CORDOBA)){
					cordoba = Integer.valueOf(bolsa.getString("pos"));
				}else if(lugar.equals(InfobolsaConstants.ANDALUCIA_GRANADA)){
					granada = Integer.valueOf(bolsa.getString("pos"));
				}else if(lugar.equals(InfobolsaConstants.ANDALUCIA_HUELVA)){
					huelva = Integer.valueOf(bolsa.getString("pos"));
				}else if(lugar.equals(InfobolsaConstants.ANDALUCIA_JAEN)){
					jaen = Integer.valueOf(bolsa.getString("pos"));
				}else if(lugar.equals(InfobolsaConstants.ANDALUCIA_MALAGA)){
					malaga = Integer.valueOf(bolsa.getString("pos"));
				}else if(lugar.equals(InfobolsaConstants.ANDALUCIA_SEVILLA)){
					sevilla = Integer.valueOf(bolsa.getString("pos"));
				}
			} catch (Exception e) {
				Log.e("error", "Error con los datos de la petición: "+e.getMessage());
			}
		}
		
		Date fecha = new Date();
		// Constrimos el objeto que se inserta
		ContentValues cv = new ContentValues();
		cv.put("general", general);
		cv.put("almeria", almeria);
		cv.put("cadiz", cadiz);
		cv.put("cordoba", cordoba);
		cv.put("granada", granada);
		cv.put("huelva", huelva);
		cv.put("jaen", jaen);
		cv.put("malaga", malaga);
		cv.put("sevilla", sevilla);
		cv.put("nif", nif);
		cv.put("fecha", fecha.getTime());
		SQLiteDatabase db = null;
		Posiciones pos = new Posiciones();
		try {
			db = SQLiteDatabase.openOrCreateDatabase(getWritableDatabase().getPath(), null) ;
			long rowid = db.insertOrThrow(InfobolsaConstants.POSITIONS_TABLE, null, cv);
			
			if(rowid > 0){
				pos = new Posiciones(general, almeria, cadiz, cordoba, granada, huelva, jaen, malaga, sevilla, nif, fecha);
			}
		} catch (Exception e) {
			Log.e("error", "savePosiciones: "+e.getMessage());
		}finally{
			if(db != null){
				db.close();
			}
		}
		return pos;
	}
	
	/**
	 * Obtiene el último movimiento de la bolsa registrado
	 * @param nif
	 * @return
	 */
	public Posiciones getLastPositionMovement(String nif){
		
		SQLiteDatabase db = null;
		Posiciones pos = null;
		Cursor cPositions = null;
		try{
			db = SQLiteDatabase.openOrCreateDatabase(getReadableDatabase().getPath(), null);
			cPositions = db.rawQuery(InfobolsaConstants.POSICIONES_SELECT_QUERY, new String[]{nif});
			if(cPositions.moveToFirst()){
				pos =  new Posiciones(cPositions);
			}
		}catch(Exception e){
			Log.e("DATABASE", "getLastPositionMovement: "+nif+ " - "+e.getMessage());
		}finally{
			if(cPositions != null){
				cPositions.close();
			}
			if(db != null){
				db.close();
			}
		}
		return pos;
	}
	
	/**
	 * Obtiene el último movimiento de la bolsa registrado
	 * @param nif
	 * @return
	 */
	public Posiciones getPreviousPositionMovement(String nif){
		
		SQLiteDatabase db = null;
		Posiciones pos = null;
		Cursor cPositions = null;
		try{
			db = SQLiteDatabase.openOrCreateDatabase(getReadableDatabase().getPath(), null);
			cPositions= db.rawQuery(InfobolsaConstants.POSICIONES_SELECT_QUERY_2, new String[]{nif});
			if(cPositions.moveToFirst()){
				do{
					pos =  new Posiciones(cPositions);
				}while(cPositions.moveToNext());
			}
		}catch(Exception e){
			Log.e("DATABASE", "getPreviousPositionMovement: "+nif+" - "+e.getMessage());
		}finally{
			if(cPositions != null){
				cPositions.close();
			}
			if(db != null){
				db.close();
			}
		}
		return pos;
	}
	
	/**
	 * Obtiene los movimientos de los 2 ultimos meses
	 * @param nif
	 * @return
	 */
	public JSONArray getLastTwoMonthMovementsUser(String nif){
		
		SQLiteDatabase db = null;
		JSONArray arrayPos = new JSONArray();
		Cursor cPositions = null;
		try{
			db = SQLiteDatabase.openOrCreateDatabase(getReadableDatabase().getPath(), null);
			Calendar today = Calendar.getInstance();
			Calendar last2Month = Calendar.getInstance();
			last2Month.add(Calendar.MONTH, -2);
			String query = InfobolsaConstants.POSICIONES_SELECT_QUERY_DATE;
			query = query.replaceAll("F1", ""+last2Month.getTime().getTime()).replaceAll("F2", ""+today.getTime().getTime());
			cPositions = db.rawQuery(query, new String[]{nif});
			if(cPositions.moveToFirst()){
				do{
					arrayPos.put(new Posiciones(cPositions).toJson());
				}while(cPositions.moveToNext());
			}
		}catch(Exception e){
			e.printStackTrace();
			Log.e("DATABASE", "getAllMovementsUser: "+nif+" - "+e.getMessage());
		}finally{
			if(cPositions != null){
				cPositions.close();
			}
			if(db != null){
				db.close();
			}
		}
		
		return arrayPos;
	}
	
	
	public JSONArray getLastMonthMovementsUser(String nif){
		
		SQLiteDatabase db = null;
		JSONArray arrayPos = new JSONArray();
		Cursor cPositions = null;
		try{
			db = SQLiteDatabase.openOrCreateDatabase(getReadableDatabase().getPath(), null);
			Calendar today = GregorianCalendar.getInstance();
			Calendar lastMonth = GregorianCalendar.getInstance();
			lastMonth.add(Calendar.DATE, -15);
			String query = InfobolsaConstants.POSICIONES_SELECT_QUERY_DATE;
			query = query.replaceAll("F1", ""+lastMonth.getTime().getTime()).replaceAll("F2", ""+today.getTime().getTime());
			cPositions = db.rawQuery(query, new String[]{nif});
			if(cPositions.moveToFirst()){
				do{
					arrayPos.put(new Posiciones(cPositions).toJson());
				}while(cPositions.moveToNext());
			}
		}catch(Exception e){
			Log.e("DATABASE", "getAllMovementsUser: "+nif+" - "+e.getMessage());
		}finally{
			if(cPositions != null){
				cPositions.close();
			}
			if(db != null){
				db.close();
			}
		}
		
		return arrayPos;
	}
	
	public JSONArray getLastFourMonthMovementsUser(String nif){
		
		SQLiteDatabase db = null;
		JSONArray arrayPos = new JSONArray();
		Cursor cPositions = null;
		try{
			db = SQLiteDatabase.openOrCreateDatabase(getReadableDatabase().getPath(), null);
			Calendar today = GregorianCalendar.getInstance();
			Calendar last4Month = GregorianCalendar.getInstance();
			last4Month.add(Calendar.MONTH, -4);
			String query = InfobolsaConstants.POSICIONES_SELECT_QUERY_DATE;
			query = query.replaceAll("F1", ""+last4Month.getTime().getTime()).replaceAll("F2", ""+today.getTime().getTime());
			cPositions = db.rawQuery(query, new String[]{nif});
			if(cPositions.moveToFirst()){
				do{
					arrayPos.put(new Posiciones(cPositions).toJson());
				}while(cPositions.moveToNext());
			}
		}catch(Exception e){
			Log.e("DATABASE", "getAllMovementsUser: "+nif+" - "+e.getMessage());
		}finally{
			if(cPositions != null){
				cPositions.close();
			}
			if(db != null){
				db.close();
			}
		}
		
		return arrayPos;
	}
	
	public JSONArray getAllMovementsUser(String nif){
		
		SQLiteDatabase db = null;
		JSONArray arrayPos = new JSONArray();
		Cursor cPositions = null;
		try{
			db = SQLiteDatabase.openOrCreateDatabase(getReadableDatabase().getPath(), null);
			cPositions = db.rawQuery(InfobolsaConstants.POSICIONES_SELECT_QUERY_ALL, new String[]{nif});
			if(cPositions.moveToFirst()){
				do{
					arrayPos.put(new Posiciones(cPositions).toJson());
				}while(cPositions.moveToNext());
			}
		}catch(Exception e){
			Log.e("DATABASE", "getAllMovementsUser: "+nif+" - "+e.getMessage());
		}finally{
			if(cPositions != null){
				cPositions.close();
			}
			if(db != null){
				db.close();
			}
		}
		
		return arrayPos;
	}
	
	/**
	 * Limpia los acentos de la cadena
	 * @param cadena
	 * @return
	 */
	private String limpiarAcentos(String cadena){
		
		return cadena.replaceAll("&aacute;", "a").replaceAll("&eacute;", "e").replaceAll("&oacute;", "o").replaceAll("&iacute;", "i");
	}
	
	/**
	 * Obtiene todas las bolsas
	 * @return
	 */
	public List<Bolsa> getAllBolsas(){
		
		List<Bolsa> bolsas = new ArrayList<Bolsa>();
		SQLiteDatabase db = null;
		Cursor cBolsas = null;
		try{
			db = SQLiteDatabase.openOrCreateDatabase(getReadableDatabase().getPath(), null);
			cBolsas = db.rawQuery(InfobolsaConstants.BOLSAS_SELECT_QUERY_ALL, null);
			if(cBolsas.moveToFirst()){
				do{
					bolsas.add(new Bolsa(cBolsas));
				}while(cBolsas.moveToNext());
			}
		}catch(Exception e){
			Log.e("DATABASE", "getAllBolsas: "+e.getMessage());
		}finally{
			if(cBolsas != null){
				cBolsas.close();
			}
			if(db != null){
				db.close();
			}
		}
		return bolsas;
	}
	
	/**
	 * actualiza la última fecha de actualuzacion
	 * @param bolsasToUpdate
	 * @return
	 */
	public boolean updateBolsasLastAct(List<Bolsa> bolsasToUpdate){
		
		SQLiteDatabase db = null;
		
		try{
			db = SQLiteDatabase.openOrCreateDatabase(getWritableDatabase().getPath(), null);
			for(Bolsa b: bolsasToUpdate){
				ContentValues cv = new ContentValues();
				cv.put("lastAct", b.getLastAct().getTime());
				cv.put("bolsatext", b.getBolsaText());
				if(db.update(InfobolsaConstants.BOLSAS_TABLE, cv, "bolsa = ?", new String[]{b.getBolsa()}) == 0){
					cv.put("bolsa", b.getBolsa());
					db.insert(InfobolsaConstants.BOLSAS_TABLE, null, cv);
				}
			}
		}catch(Exception e){
			Log.e("DATABASE", "updateBolsasLastAct: "+e.getMessage());
		}finally{
			if(db != null){
				db.close();
			}
		}
		
		return true;
	}
	
	/**
	 * Obtiene todas las bolsas
	 * @return
	 */
	public List<String> getAllDnis(String bolsa){
		
		List<String> dnis = new ArrayList<String>();
		SQLiteDatabase db = null;
		Cursor cDnis = null;
		try{
			db = SQLiteDatabase.openOrCreateDatabase(getReadableDatabase().getPath(), null);
			cDnis = db.rawQuery(InfobolsaConstants.DNIS_FROM_BOLSAS_SELECT_QUERY, new String[]{bolsa});
			if(cDnis.moveToFirst()){
				do{
					dnis.add(cDnis.getString(cDnis.getColumnIndex("nif")));
				}while(cDnis.moveToNext());
			}
		}catch(Exception e){
			Log.e("DATABASE", "getAllDnis: "+bolsa+" - "+e.getMessage());
		}finally{
			if(cDnis != null){
				cDnis.close();
			}
			if(db != null){
				db.close();
			}
		}
		return dnis;
	}
	
	/**
	 * Simula la segunda petición sin llamar a la consejería
	 * @param nif
	 * @return
	 */
	public User obtenerSecondFase(String nif){
		
		User us = new User();
		SQLiteDatabase db = null;
		Cursor cUser = null;
		try{
			db = SQLiteDatabase.openOrCreateDatabase(getReadableDatabase().getPath(), null);
			cUser = db.rawQuery(InfobolsaConstants.SIMULATE_SECOND_SELECT_QUERY, new String[]{nif});
			List<Posiciones> positions = new ArrayList<Posiciones>();
			if(cUser.moveToFirst()){
				do{
					positions.add(new Posiciones(cUser));
					us = new User(cUser);
				}while(cUser.moveToNext());
				us.setPosiciones(positions);
			}
		}catch(Exception e){
			Log.e("DATABASE", "obtenerSecondFase: "+nif+" - "+e.getMessage());
		}finally{
			if(cUser != null){
				cUser.close();
			}
			if(db != null){
				db.close();
			}
		}
		return us;
	}
	
	
	public boolean exportDatabase() throws IOException {

		close();
		Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
		if(isSDPresent){
			File externalStorage = new File(android.os.Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+InfobolsaConstants.DATABASE_NAME);
			InputStream imp = new FileInputStream(new File(getReadableDatabase().getPath()));
			copyFile(imp , new FileOutputStream(externalStorage));
		}else if(android.os.Environment.DIRECTORY_DOWNLOADS != null){
			File downStorage = new File(android.os.Environment.DIRECTORY_DOWNLOADS);
			InputStream imp = new FileInputStream(new File(getReadableDatabase().getPath()));
			copyFile(imp , new FileOutputStream(downStorage));
		}
		return true;
	}
	
	public boolean importDatabase(String dbPath) throws IOException {

	    // Close the SQLiteOpenHelper so it will commit the created empty
	    // database to internal storage.
	    close();
	    File newDb = new File(dbPath);
	   // File oldDb = new File(getReadableDatabase().getPath());
	    if (newDb.exists()) {
	        // it as created.
	        getWritableDatabase().close();
	        return true;
	    }
	    return false;
	}
	
	private void copyFile(InputStream in, OutputStream out) throws IOException {
	    byte[] buffer = new byte[1024];
	    int read;
	    while((read = in.read(buffer)) != -1){
	      out.write(buffer, 0, read);
	    }
	}
	
}
