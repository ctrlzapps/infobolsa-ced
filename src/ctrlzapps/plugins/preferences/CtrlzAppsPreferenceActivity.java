/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps.plugins.preferences;

import ctrlzapps.CtrlzAppsUtils;
import ctrlzapps.alarm.manager.AlarmControl;
import ctrlzapps.infobolsaced.R;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

/**
 * Actividad que controla las Preferencias
 * @author ctrlzapps
 *
 */
public class CtrlzAppsPreferenceActivity extends PreferenceActivity{

	/**
	 * Cuando se crea la actividad
	 * @param savedInstanceState
	 */
	@SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		addPreferencesFromResource(R.xml.preferences);
		int elementIndex = CtrlzAppsUtils.getIndex(getResources().getStringArray(R.array.frecuenciasValues),
				PreferenceManager.getDefaultSharedPreferences(this).getString("frecuenciaid", "900000"));
		String titleSelected = getResources().getStringArray(R.array.frecuenciasTitle)[elementIndex];
		
		ListPreference lp = (ListPreference) findPreference("frecuenciaid");
		lp.setSummary("Tiempo de refresco: "+titleSelected);
		lp.setOnPreferenceChangeListener(new OnFrecuencyChangeListener());
		CheckBoxPreference chkp = (CheckBoxPreference) findPreference("notification");
		chkp.setOnPreferenceChangeListener(new OnNotificationChangeListener());
    }
	
	/**
	 * Clase que se encarga de controlar cuando el ListPreferences cambia de valor
	 * @author stejeros
	 *
	 */
	public class OnFrecuencyChangeListener implements Preference.OnPreferenceChangeListener{
		
		@SuppressWarnings("deprecation")
		public boolean onPreferenceChange(Preference paramPreference, Object newObject){
			int elementIndex = CtrlzAppsUtils.getIndex(getResources().getStringArray(R.array.frecuenciasValues), newObject.toString());
	
			if (elementIndex > -1){
				String titleSelected = getResources().getStringArray(R.array.frecuenciasTitle)[elementIndex];
				ListPreference lp = (ListPreference) findPreference("frecuenciaid");
				lp.setSummary("Tiempo de refresco: "+titleSelected);
			}
			
			// Además hay que configurar la ALARMMANAGER
			AlarmControl.startUpdateAlarm(CtrlzAppsPreferenceActivity.this, Integer.valueOf(newObject.toString()));
			return true;
		}
	}
	
	public class OnNotificationChangeListener implements Preference.OnPreferenceChangeListener{
		
		public boolean onPreferenceChange(Preference paramPreference, Object newObject){
						
			Boolean activo = (Boolean) newObject;
			if(activo){
				Integer refresco = Integer.valueOf(PreferenceManager
						.getDefaultSharedPreferences(CtrlzAppsPreferenceActivity.this)
						.getString("frecuenciaid", "900000"));
				AlarmControl.startUpdateAlarm(CtrlzAppsPreferenceActivity.this, refresco);
			}else{
				AlarmControl.cancelAlarm(CtrlzAppsPreferenceActivity.this);
			}
			return true;
		}
	}
	
	@Override
    public void onConfigurationChanged(Configuration nuevaconfig) {
		 super.onConfigurationChanged(nuevaconfig);
    }
}
