/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps.plugins.statusBarNotification;

import java.util.Random;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import ctrlzapps.infobolsaced.InfoBolsaCEDActivity;
import ctrlzapps.infobolsaced.R;

/**
 * Intentet para lanzar notificaciones
 * @author ctrlzapps
 *
 */
public class StatusNotificationIntent {
    
	/**
	 * 
	 * @param context Contexto de la aplicación
	 * @param id ID de la notificación
	 * @param contentTitle Título de la notificación
	 * @param contentText Cuerpo de la notificación
	 * @return
	 */
	
	@SuppressWarnings("deprecation")
	public static Notification buildNotification(Context context, CharSequence id, CharSequence contentTitle, CharSequence contentText ) {
    	    	
		//TODO: ver como arreglar la chapuza que hemos hecho.
		Intent notificationIntent = new Intent (context, InfoBolsaCEDActivity.class);
		Random random = new Random();
		
		notificationIntent.putExtra("notificationID", id);
    	notificationIntent.setFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
    	PendingIntent contentIntent = PendingIntent.getActivity(context, random.nextInt(), notificationIntent, PendingIntent.FLAG_ONE_SHOT);
    	
    	Notification notif = new Notification(R.drawable.notification, contentText, System.currentTimeMillis());
    	notif.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
    	notif.flags = Notification.FLAG_AUTO_CANCEL;
    	
    	int defaults = Notification.DEFAULT_LIGHTS;
    	if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("vibrate", Boolean.FALSE)){
    		defaults = defaults | Notification.DEFAULT_VIBRATE;
    	}
    	if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("sound", Boolean.FALSE)){
    		defaults = defaults | Notification.DEFAULT_SOUND;
    	}
    	
    	notif.defaults = defaults;
        return notif;
        
    }
}
