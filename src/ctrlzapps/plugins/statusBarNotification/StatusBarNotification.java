/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps.plugins.statusBarNotification;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.PluginResult;
import org.apache.cordova.api.PluginResult.Status;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.util.Log;

/**
 * Clase para las notificaciones
 * @author ctrlzapps
 *
 */
public class StatusBarNotification extends CordovaPlugin {
    //	Action to execute
    public static final String NOTIFY = "notify";
    public static final String CLEAR = "clear";
    
    private NotificationManager mNotificationManager;
    

    /**
     * 	Executes the request and returns PluginResult
     *
     * 	@param action		Action to execute
     * 	@param data			JSONArray of arguments to the plugin
     *  @param callbackId	The callback id used when calling back into JavaScript
     *
     *  @return				A PluginRequest object with a status
     * */
    @Override
    public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) throws JSONException {
        PluginResult result = null;
        if (NOTIFY.equals(action)) {
            try {
                String idNot = args.getString(0);
                String title = args.getString(1);
                String body = args.getString(2);
                Log.d("NotificationPlugin", "Notification: " + idNot + ", " + title + ", " + body);
                showNotification(this.webView.getContext(), idNot, title, body);
                result = new PluginResult(Status.OK);
            } catch (JSONException jsonEx) {
                Log.e("NotificationPlugin", "JSON Exception "
                        + jsonEx.getMessage());
                result = new PluginResult(Status.JSON_EXCEPTION);
            }
        } else if (CLEAR.equals(action)){
            try {
                String idNot = args.getString(0);
                Log.d("NotificationPlugin", "Notification cancel: " + idNot);
                clearNotification(idNot);
            } catch (JSONException jsonEx) {
                Log.e("NotificationPlugin", "JSON Exception " + jsonEx.getMessage());
                result = new PluginResult(Status.JSON_EXCEPTION);
            }
        } else {
            result = new PluginResult(Status.INVALID_ACTION);
            Log.d("NotificationPlugin", "Invalid action : "+action+" passed");
        }
        callbackContext.sendPluginResult(result);
        return true;
    }

    /**
     * 	Displays status bar notification
     *
     * 	@param id Notification ID.
     *  @param contentTitle	Notification title
     *  @param contentText	Notification text
     * */
    public void showNotification(Context context, CharSequence id, CharSequence contentTitle, CharSequence contentText) {
    	try{
    		Log.d("ShowNotification", "Notificación: " + id + " - " + contentText);
        
            mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            Notification noti = StatusNotificationIntent.buildNotification(context, id, contentTitle, contentText);
            mNotificationManager.notify(id.hashCode(), noti);

    	}catch(Exception e){
    		Log.e("Error mostrando notificación", e.getMessage());
    	}
    	
    }

    /**
     * Cancels a single notification by id.
     *
     * @param id Notification id to cancel.
     */
    public void clearNotification(String id) {
        mNotificationManager.cancel(id.hashCode());
    }

    /**
     * Removes all Notifications from the status bar.
     */
    public void clearAllNotifications() {
        mNotificationManager.cancelAll();
    }
    
}
