/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps.plugins.statusBarNotification;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;

/**
 * Notificaciones
 * @author ctrlzapps
 * 
 */
public class StatusBarNotificationAction extends Activity {

	/**
	 * 
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		onNewIntent(getIntent());
	}

	
	@Override
    public void onConfigurationChanged(Configuration nuevaconfig) {
		 super.onConfigurationChanged(nuevaconfig);
    }
	/**
	 * 
	 
	@Override
	public void onNewIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		if (extras != null) {
			if (extras.containsKey("notificationID")) {
				String idNot = extras.get("notificationID").toString();
				Log.d("statusBarNotification", "Pulsada notificación: " + idNot);
				DataBase db = new DataBase(this);
				String bolsa = db.getUserByNif(idNot).getBolsa();
				// super.appView.loadUrl("javascript:hacerPeticion('" +idNot +
				// "','" + bolsa + "');");
				//super.sendJavascript("hacerPeticion('" + idNot + "','" + bolsa + "');");
				// TODO: llamar al método de salva
			} else {
				Log.e("statusBarNotification", "La notificación no tiene ID");
			}
		}

	}
*/
}
