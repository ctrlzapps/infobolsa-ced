/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps.plugins;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

import com.google.analytics.tracking.android.GAServiceManager;
import com.google.analytics.tracking.android.GoogleAnalytics;

/**
 * Plugin para Google Analytics
 * @author ctrlzapps
 *
 */
public class GoogleAnalyticsPlugin extends CordovaPlugin {

	
	@Override
	public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) throws JSONException {
		try {
            if (action.equals("sendChangeView")) {
            	String idPage = args.getString(0);
            	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, sendChangeView(idPage)));
            	return true;
            }else {
            	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.INVALID_ACTION));
            	return false;
            }
        } catch (JSONException e) {
        	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.JSON_EXCEPTION));
        	return false;
        } catch (Exception e) {
        	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR));
        	return false;
		}
	}
	
	/**
	 * Marca una página en Google Analytics
	 * @param idPage
	 */
	private Boolean sendChangeView(String idPage){
		
		GoogleAnalytics myInstance = GoogleAnalytics.getInstance(this.webView.getContext());
		myInstance.getDefaultTracker().sendView("/"+idPage);
		GAServiceManager.getInstance().dispatch();
		return true;
	}
}
