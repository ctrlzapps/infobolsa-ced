/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps.plugins;

import java.io.IOException;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ctrlzapps.infobolsaced.database.DataBase;
import android.util.Log;

/**
 * Clase que se encargará de listar los usuarios de la BBDD
 * @author ctrlzapps
 *
 */
public class ConsultaUserPlugin extends CordovaPlugin { 
	
	/**
	 * Obtiene todos los usuarios para mostrarlos
	 * @return
	 */
	public JSONObject getUsers(){
		
		JSONObject retorno = new JSONObject();
		DataBase db = new DataBase(this.webView.getContext());
		
		try {
			retorno.put("users", db.getAllUsers());
		} catch (JSONException e) {
			Log.e("error", "error construyendo array");
		} finally {
			db.close();
		}
		return retorno;
	}
	
	public JSONObject getMovementsUser(String nif, Integer numberMonth){
		
		JSONObject retorno = new JSONObject();
		DataBase db = new DataBase(this.webView.getContext());
		
		try {
			if(numberMonth == null || numberMonth == 0){
				retorno.put("positions", db.getAllMovementsUser(nif));
			}else if(numberMonth == 2){
				retorno.put("positions", db.getLastTwoMonthMovementsUser(nif));
			}else if(numberMonth == 4){
				retorno.put("positions", db.getLastFourMonthMovementsUser(nif));
			}else if(numberMonth == 1){
				retorno.put("positions", db.getLastMonthMovementsUser(nif));
			}
		} catch (JSONException e) {
			Log.e("error", "error construyendo array");
		} finally {
			db.close();
		}
		return retorno;
	}
	
	/**
	 * Elimina el usuario con el nif
	 * @param nif
	 * @return
	 */
	public JSONObject deleteUser(String nif){
		
		JSONObject retorno = new JSONObject();
		if(nif != null && !nif.equals("")){
			String nifAux = nif.toUpperCase();
			DataBase db = new DataBase(this.webView.getContext());
			try {
			
				if(db.deleteUser(nifAux)){
					retorno = getUsers();
				}else{
					retorno.put("result", "false");
				}
			} catch (JSONException e) {
				Log.e("error", "error eliminando el usuario");
			} finally {
				db.close();
			}
		}
		
		return retorno;
	}

	public boolean exportDatabase(){
	
		boolean retorno = false;
		DataBase db = new DataBase(this.webView.getContext());
		try {
		
			if(db.exportDatabase()){
				retorno = true;
			}else{
				retorno = false;
			}
		} catch (IOException e) {
			Log.e("error", "exportando BBDD");
		} finally {
			db.close();
		}
		return retorno;
	}
	
	@Override
	public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) throws JSONException {
		try {
            if (action.equals("allUsers")) {
            	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getUsers()));
            	return true;
            } else if (action.equals("deleteUser")){
            	String NIF = args.getString(0);
            	 
                if (NIF != null && NIF.length() > 0){
                	NIF = NIF.toUpperCase();
                	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, deleteUser(NIF)));
                	return true;
                } else {
                	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR));
                	return false;
                }
            } else if (action.equals("allmovementsuser")){
            	String NIF = args.getString(0);
            	 
                if (NIF != null && NIF.length() > 0){
                	NIF = NIF.toUpperCase();
                	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getMovementsUser(NIF, null)));
                	return true;
                } else {
                	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR));
                	return false;
                }
            } else if (action.equals("lasttwomovementsuser")){
            	String NIF = args.getString(0);
            	 
                if (NIF != null && NIF.length() > 0){
                	NIF = NIF.toUpperCase();
                	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getMovementsUser(NIF, 2)));
                	return true;
                } else {
                	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR));
                	return false;
                }
            }else if (action.equals("lastfourmovementsuser")){
            	String NIF = args.getString(0);
            	 
                if (NIF != null && NIF.length() > 0){
                	NIF = NIF.toUpperCase();
                	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getMovementsUser(NIF, 4)));
                	return true;
                } else {
                	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR));
                	return false;
                }
            }else if (action.equals("lastmovementsuser")){
            	String NIF = args.getString(0);
            	 
                if (NIF != null && NIF.length() > 0){
                	NIF = NIF.toUpperCase();
                	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getMovementsUser(NIF, 1)));
                	return true;
                } else {
                	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR));
                	return false;
                }
            }else if (action.equals("exportdatabase")){
            	if(exportDatabase()){
            		callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, ""));
            		return true;
            	}else{
            		callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR));
            		return false;
            	}
            }else {
            	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.INVALID_ACTION));
            	return false;
            }
        } catch (JSONException e) {
        	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.JSON_EXCEPTION));
        	return false;
        } catch (Exception e) {
        	callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR));
        	return false;
		}
	}

}
