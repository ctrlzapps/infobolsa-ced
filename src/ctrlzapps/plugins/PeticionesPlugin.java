/*
 * 
 * Aplicación para la consulta de la posición en la bolsa de maestros y profesores interinos
 * de la Consejería de Educación de la Junta de Andalucía.
 * Copyright (C) 2013  Ctrlzapps.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package ctrlzapps.plugins;

import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.PluginResult;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import android.content.Context;
import android.util.Log;

import ctrlzapps.CtrlzAppsGzipDescompressingEntity;
import ctrlzapps.CtrlzAppsUtils;
import ctrlzapps.beans.Bolsa;
import ctrlzapps.beans.Posiciones;
import ctrlzapps.beans.User;
import ctrlzapps.infobolsaced.InfobolsaConstants;
import ctrlzapps.infobolsaced.database.DataBase;

/**
 * Plugin para realizar las peticiones a la CEJA
 * @author ctrlzapps
 *
 */
public class PeticionesPlugin extends CordovaPlugin {

	@Override
	public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) throws JSONException {
		try {
			if (action.equals("lastAct")) {
				String idBolsa = args.getString(0);
				if (idBolsa != null && idBolsa.length() > 0) {
					callbackContext.sendPluginResult(new PluginResult(
							PluginResult.Status.OK, getLastAct(idBolsa)));
					return true;
				} else {
					callbackContext.sendPluginResult(new PluginResult(
							PluginResult.Status.ERROR));
					return false;
				}
			} else if (action.equals("infoBolsa")) {
				String DNI = args.getString(0);
				String idBolsa = args.getString(1);
				if (DNI != null && DNI.length() > 0) {
					if (idBolsa != null && idBolsa.equals("NOTI")) {
						idBolsa = null;
					}
					callbackContext.sendPluginResult(new PluginResult(
							PluginResult.Status.OK, getBolsasData(DNI, idBolsa,
									args.getString(2), args.getString(3))));
					return true;
				} else {
					callbackContext.sendPluginResult(new PluginResult(
							PluginResult.Status.ERROR));
					return false;
				}
			} else {
				callbackContext.sendPluginResult(new PluginResult(
						PluginResult.Status.INVALID_ACTION));
				return false;
			}
		} catch (JSONException e) {
			callbackContext.sendPluginResult(new PluginResult(
					PluginResult.Status.JSON_EXCEPTION));
			return false;
		} catch (Exception e) {
			callbackContext.sendPluginResult(new PluginResult(
					PluginResult.Status.ERROR));
			return false;
		}
	}

	public JSONObject getLastAct(String idBolsa) {

		JSONObject lastAct = new JSONObject();
		try {

			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is
			// established.
			int timeoutConnection = 10000;
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 10000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

			DefaultHttpClient localDefaultHttpClient = new DefaultHttpClient(
					httpParameters);
			addGzipInterceptors(localDefaultHttpClient);
			HttpPost localHttpPost = new HttpPost(
					InfobolsaConstants.UrlBolsa.INDEX.toString().replace(
							"[idBolsa]", idBolsa));
			localHttpPost.addHeader(
					"Referer",
					InfobolsaConstants.UrlBolsa.BOLSAS.toString().replace(
							"[idBolsa]", idBolsa));
			Document pagActualizacion = Jsoup.parse(EntityUtils
					.toString(localDefaultHttpClient.execute(localHttpPost)
							.getEntity()));
			String fechaHoraAct = pagActualizacion.getElementsByTag("table")
					.get(2).getElementsByTag("td").first().text();

			// obtenemos la fecha
			Pattern pathInfoMask = Pattern
					.compile(InfobolsaConstants.DATE_PATTERN);
			Matcher pathMatcher = pathInfoMask.matcher(fechaHoraAct);
			if (pathMatcher.find()) {
				lastAct.put("textofecha", pathMatcher.group());
			}
			// obtenemos la hora
			pathInfoMask = Pattern.compile(InfobolsaConstants.HOUR_PATTERN);
			pathMatcher = pathInfoMask.matcher(fechaHoraAct);
			if (pathMatcher.find()) {
				lastAct.put("hora", pathMatcher.group());
			}
			// System.gc();
		} catch (Exception e) {
			Log.e("lastact",
					"Error realizando la consulta de última actualización");
		}

		return lastAct;

	}

	public JSONObject getBolsasData(String DNI, String idBolsa,
			String salvarDatos, Context context) {
		return getBolsasData(DNI, idBolsa, salvarDatos, context, null);
	}

	public JSONObject getBolsasData(String DNI, String idBolsa,
			String salvarDatos, String notification) {
		return getBolsasData(DNI, idBolsa, salvarDatos, null, notification);
	}

	/**
	 * Obtiene los datos de la bolsa
	 *
	 * @param DNI
	 * @param idBolsa
	 * @return
	 */
	public JSONObject getBolsasData(String DNI, String idBolsa,
			String salvarDatos, Context context, String notifications) {

		JSONObject returnJSON = new JSONObject();
		DataBase db = null;
		if (context == null) {
			db = new DataBase(this.webView.getContext());
			context = this.webView.getContext();
		} else {
			db = new DataBase(context);
		}

		try {

			User us = db.getUserByNif(DNI.toUpperCase());
			if (us != null && (idBolsa == null || idBolsa.equals(""))) {
				idBolsa = us.getBolsa();
			}
			if (CtrlzAppsUtils.networkAvailable(context)) {// Comprobamos que
															// haya conexión a
															// internet
				HttpParams httpParameters = new BasicHttpParams();
				int timeoutConnection = 20000;
				HttpConnectionParams.setConnectionTimeout(httpParameters,
						timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT)
				// in milliseconds which is the timeout for waiting for data.
				int timeoutSocket = 20000;
				HttpConnectionParams
						.setSoTimeout(httpParameters, timeoutSocket);
				DefaultHttpClient localDefaultHttpClient = new DefaultHttpClient(
						httpParameters);
				HttpPost localHttpPost = new HttpPost(
						InfobolsaConstants.UrlBolsa.INDEX.toString().replace(
								"[idBolsa]", idBolsa));
				localHttpPost.addHeader("Referer",
						InfobolsaConstants.UrlBolsa.BOLSAS.toString());
				EntityUtils.getContentCharSet(localDefaultHttpClient.execute(
						localHttpPost).getEntity());
				localHttpPost
						.addHeader("User-Agent",
								"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");

				localHttpPost = new HttpPost(InfobolsaConstants.UrlBolsa.VER
						.toString().replace("[idBolsa]", idBolsa));
				localHttpPost.addHeader(
						"Referer",
						InfobolsaConstants.UrlBolsa.INDEX.toString().replace(
								"[idBolsa]", idBolsa));
				localHttpPost
						.addHeader("User-Agent",
								"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
				LinkedList<BasicNameValuePair> listParams = new LinkedList<BasicNameValuePair>();

				listParams
						.add(new BasicNameValuePair("DNI", DNI.toUpperCase()));
				listParams.add(new BasicNameValuePair("X", "X"));
				addGzipInterceptors(localDefaultHttpClient);
				localHttpPost.setEntity(new UrlEncodedFormEntity(listParams));

				// Primera petición

				Document firstPage = null;

				if (us == null) {
					firstPage = Jsoup.parse(EntityUtils
							.toString(localDefaultHttpClient.execute(
									localHttpPost).getEntity()));
					Elements datos = firstPage.getElementsByAttributeValue(
							"name", "F1").select("input[type=hidden]");
					if (!datos.isEmpty()) {
						String cue = "";
						String esp = "";
						String x = "";
						listParams = new LinkedList<BasicNameValuePair>();
						listParams.add(new BasicNameValuePair("DNI", DNI
								.toUpperCase()));
						String name = firstPage
								.getElementsByAttributeValue("name", "F1")
								.select("a").text();
						for (Element input : datos) {
							if (input.attr("name").toUpperCase().equals("CUE")) {
								listParams.add(new BasicNameValuePair("CUE",
										input.val()));
								cue = input.val();
							} else if (input.attr("name").toUpperCase()
									.equals("ESP")) {
								listParams.add(new BasicNameValuePair("ESP",
										input.val()));
								listParams.add(new BasicNameValuePair("PUESTO",
										input.val()));
								esp = input.val();
							} else if (input.attr("name").toUpperCase()
									.equals("X")
									|| input.attr("name").toUpperCase()
											.equals("ID")) {
								listParams.add(new BasicNameValuePair("X",
										input.val()));
								listParams.add(new BasicNameValuePair("ID",
										input.val()));
								x = input.val();
							}
						}

						String bolsatext = firstPage.select(
								"table:eq(0) td.campn:eq(3)").text();
						if (bolsatext != null && bolsatext.indexOf(")") > 0) {
							bolsatext = bolsatext.substring(bolsatext
									.indexOf(")") + 1);
						}
						if (bolsatext != null && bolsatext.indexOf("--") > 0) {
							bolsatext = bolsatext.substring(0,
									bolsatext.indexOf("--"));
						}
						if (bolsatext != null && bolsatext.indexOf(";") > 0) {
							bolsatext = bolsatext.split(";")[1];
						}
						if (salvarDatos != null && salvarDatos.equals("true")) {
							us = db.saveData(DNI.toUpperCase(), cue, esp, x,
									name, idBolsa, bolsatext);
							List<Bolsa> lBolsa = new ArrayList<Bolsa>();
							lBolsa.add(new Bolsa(idBolsa, bolsatext, new Date()));
							db.updateBolsasLastAct(lBolsa);
						}
					}
				} else {
					firstPage = Jsoup.parse(EntityUtils
							.toString(localDefaultHttpClient.execute(
									localHttpPost).getEntity()));
					// EntityUtils.getContentCharSet(localDefaultHttpClient.execute(localHttpPost).getEntity());
					listParams = new LinkedList<BasicNameValuePair>();
					listParams.add(new BasicNameValuePair("DNI", DNI
							.toUpperCase()));
					listParams.add(new BasicNameValuePair("CUE", us.getCue()));
					listParams.add(new BasicNameValuePair("ESP", us.getEsp()));
					listParams
							.add(new BasicNameValuePair("PUESTO", us.getEsp()));
					listParams.add(new BasicNameValuePair("X", us.getX()));
					listParams.add(new BasicNameValuePair("ID", us.getX()));
				}
				// Se ha creado o ya hay usuario
				if (us != null && us.getNif() != null) {

					addGzipInterceptors(localDefaultHttpClient);
					localHttpPost.removeHeaders("Referer");
					localHttpPost.addHeader(
							"Referer",
							InfobolsaConstants.UrlBolsa.VER.toString().replace(
									"[idBolsa]", idBolsa));
					localHttpPost.setURI(new URI(
							InfobolsaConstants.UrlBolsa.SELDNI.toString()
									.replace("[idBolsa]", idBolsa)));
					localHttpPost
							.addHeader("User-Agent",
									"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
					localHttpPost
							.setEntity(new UrlEncodedFormEntity(listParams));
					Document secondPage = Jsoup.parse(EntityUtils
							.toString(localDefaultHttpClient.execute(
									localHttpPost).getEntity()));
					try {
						String[] datosPrimero = secondPage
								.select("table:eq(0) td.campn").text()
								.replaceAll("&nbsp;", "").split(" ");
						JSONObject primeroBolsa = new JSONObject();
						primeroBolsa.put("apellnom",
								secondPage.select("table:eq(0) td.campn_left")
										.text());
						primeroBolsa.put("nif", datosPrimero[0]);
						primeroBolsa.put("tservi", datosPrimero[1]);
						primeroBolsa.put("anyoopo", datosPrimero[3]);
						primeroBolsa.put("notaopo", datosPrimero[5]);

						datosPrimero = null; // aligeramos memoria

						Elements datosYomismo = secondPage
								.select("table:eq(0) td.sinraya:eq(1)");
						JSONObject yoMismo = new JSONObject();
						for (int i = 0; i < datosYomismo.size(); i++) {

							switch (i) {
							case 0:
								yoMismo.put("apellnom", datosYomismo.get(i)
										.text());
								break;
							case 1:
								yoMismo.put("nif", datosYomismo.get(i).text());
								break;
							case 2:
								yoMismo.put("tservi", datosYomismo.get(i)
										.text());
								break;
							case 3:
								yoMismo.put("anyoopo", datosYomismo.get(i)
										.text());
								break;
							case 4:
								yoMismo.put("notaopo", datosYomismo.get(i)
										.text());
								break;
							default:
								break;
							}
						}
						datosYomismo = null; // aligeramos memoria

						// Obtenemos el último moviemiento de la bolsa
						Posiciones lastPosMov = null;
						if (notifications == null
								|| !notifications
										.equals(InfobolsaConstants.FROM_NOTIFICATIONS)) {
							lastPosMov = db
									.getLastPositionMovement(us.getNif());
						} else {
							lastPosMov = db.getPreviousPositionMovement(us
									.getNif());
						}
						String[] datosBolsas = secondPage
								.select("table:gt(1) th.etiq_cab_left").html()
								.replaceAll("<br />", ",").split("\n");

						JSONArray bolsasArray = new JSONArray();
						boolean hasChange = false;
						for (String strBolsa : datosBolsas) {
							JSONObject bolsa = new JSONObject();
							String lugar = strBolsa.split(",")[0];
							Integer position = Integer.valueOf(strBolsa
									.split(",")[1]);
							Integer change = 0;
							if (lastPosMov != null) {// hay ya una consulta
														// realizada
								String lugarAux = limpiarAcentos(lugar
										.toLowerCase());
								if (lugarAux
										.equals(InfobolsaConstants.ANDALUCIA_ALMERIA)) {
									change = (-1 * (lastPosMov.getAlmeria() - position));
								} else if (lugarAux
										.equals(InfobolsaConstants.ANDALUCIA_CADIZ)) {
									change = (-1 * (lastPosMov.getCadiz() - position));
								} else if (lugarAux
										.equals(InfobolsaConstants.ANDALUCIA_CORDOBA)) {
									change = (-1 * (lastPosMov.getCordoba() - position));
								} else if (lugarAux
										.equals(InfobolsaConstants.ANDALUCIA_GRANADA)) {
									change = (-1 * (lastPosMov.getGranada() - position));
								} else if (lugarAux
										.equals(InfobolsaConstants.ANDALUCIA_HUELVA)) {
									change = (-1 * (lastPosMov.getHuelva() - position));
								} else if (lugarAux
										.equals(InfobolsaConstants.ANDALUCIA_JAEN)) {
									change = (-1 * (lastPosMov.getJaen() - position));
								} else if (lugarAux
										.equals(InfobolsaConstants.ANDALUCIA_MALAGA)) {
									change = (-1 * (lastPosMov.getMalaga() - position));
								} else if (lugarAux
										.equals(InfobolsaConstants.ANDALUCIA_SEVILLA)) {
									change = (-1 * (lastPosMov.getSevilla() - position));
								}
							}
							if (change != 0) {
								hasChange = true;
							}
							bolsa.put("lugar", lugar);
							bolsa.put("pos", position);
							bolsa.put("change", change);
							bolsasArray.put(bolsa);
						}

						String posicionGlobal = secondPage
								.select("table:eq(0) td.sinraya b").get(0)
								.text().replaceAll("lugar nº ", "");

						secondPage = null; // aligeramos memoria
						Integer posGlobal = Integer.valueOf(posicionGlobal);

						Integer posGlobalDif = (lastPosMov != null) ? (-1 * (lastPosMov
								.getGeneral() - posGlobal)) : 0;

						/*
						 * if(lastPosMov != null && posGlobalDif == 0 &&
						 * !hasChange){ long cuatrodias = 345600000; long today
						 * = new Date().getTime(); long lastChangeLong =
						 * lastPosMov.getFecha().getTime(); if(today -
						 * lastChangeLong > cuatrodias){ hasChange = true; } }
						 */
						if (lastPosMov == null || posGlobalDif != 0
								|| hasChange) {
							if (notifications == null
									|| !notifications
											.equals(InfobolsaConstants.FROM_NOTIFICATIONS)) {
								lastPosMov = db.savePosiciones(bolsasArray,
										posicionGlobal, us.getNif());
							}
						}
						if (lastPosMov != null && lastPosMov.getFecha() != null) {
							SimpleDateFormat formato = new SimpleDateFormat(
									InfobolsaConstants.DATEPARSE);
							returnJSON.put("lastChangeDate",
									formato.format(lastPosMov.getFecha()));
						} else {
							SimpleDateFormat formato = new SimpleDateFormat(
									InfobolsaConstants.DATEPARSE);
							returnJSON.put("lastChangeDate",
									formato.format(new Date()));
						}
						returnJSON.put("primero", primeroBolsa);
						returnJSON.put("posicionglobal", posicionGlobal);
						returnJSON.put("posicionglobaldif", posGlobalDif);

						returnJSON.put("yomismo", yoMismo);
						returnJSON.put("bolsas", bolsasArray);

					} catch (Exception e) {
						JSONObject yoMismo = new JSONObject();
						yoMismo.put("apellnom", us.getName());
						yoMismo.put("nif", us.getNif());
						returnJSON.put("yomismo", yoMismo);
						Posiciones lastPosMov = db.getLastPositionMovement(us
								.getNif());
						SimpleDateFormat formato = new SimpleDateFormat(
								InfobolsaConstants.DATEPARSE);
						returnJSON.put("lastChangeDate",
								formato.format(lastPosMov.getFecha()));
						JSONArray bolsasArray = new JSONArray();
						for (int i = 0; i < 8; i++) {
							JSONObject bolsa = new JSONObject();
							switch (i) {
							case 0:
								if (lastPosMov.getAlmeria() > 0) {
									bolsa.put("lugar", "Almería");
									bolsa.put("pos", lastPosMov.getAlmeria());
									bolsa.put("change", 0);
									bolsasArray.put(bolsa);
								}
								break;
							case 1:
								if (lastPosMov.getCadiz() > 0) {
									bolsa.put("lugar", "Cádiz");
									bolsa.put("pos", lastPosMov.getCadiz());
									bolsa.put("change", 0);
									bolsasArray.put(bolsa);
								}
								break;
							case 2:
								if (lastPosMov.getCordoba() > 0) {
									bolsa.put("lugar", "Córdoba");
									bolsa.put("pos", lastPosMov.getCordoba());
									bolsa.put("change", 0);
									bolsasArray.put(bolsa);
								}
								break;
							case 3:
								if (lastPosMov.getGranada() > 0) {
									bolsa.put("lugar", "Granada");
									bolsa.put("pos", lastPosMov.getGranada());
									bolsa.put("change", 0);
									bolsasArray.put(bolsa);
								}
								break;
							case 4:
								if (lastPosMov.getHuelva() > 0) {
									bolsa.put("lugar", "Huelva");
									bolsa.put("pos", lastPosMov.getHuelva());
									bolsa.put("change", 0);
									bolsasArray.put(bolsa);
								}
								break;
							case 5:
								if (lastPosMov.getJaen() > 0) {
									bolsa.put("lugar", "Jaén");
									bolsa.put("pos", lastPosMov.getJaen());
									bolsa.put("change", 0);
									bolsasArray.put(bolsa);
								}
								break;
							case 6:
								if (lastPosMov.getMalaga() > 0) {
									bolsa.put("lugar", "Málaga");
									bolsa.put("pos", lastPosMov.getMalaga());
									bolsa.put("change", 0);
									bolsasArray.put(bolsa);
								}
								break;
							case 7:
								if (lastPosMov.getSevilla() > 0) {
									bolsa.put("lugar", "Sevilla");
									bolsa.put("pos", lastPosMov.getSevilla());
									bolsa.put("change", 0);
									bolsasArray.put(bolsa);
								}
								break;
							default:
								break;
							}

						}
						returnJSON.put("posicionglobal",
								lastPosMov.getGeneral());
						returnJSON.put("bolsas", bolsasArray);
						returnJSON.put("lastChangeDate",
								formato.format(lastPosMov.getFecha()));
					}

				}

			} else {
				// No hay conexion
				JSONObject yoMismo = new JSONObject();
				yoMismo.put("apellnom", us.getName());
				yoMismo.put("nif", us.getNif());
				returnJSON.put("yomismo", yoMismo);
				Posiciones lastPosMov = db.getLastPositionMovement(us.getNif());
				SimpleDateFormat formato = new SimpleDateFormat(
						InfobolsaConstants.DATEPARSE);
				returnJSON.put("lastChangeDate",
						formato.format(lastPosMov.getFecha()));
				JSONArray bolsasArray = new JSONArray();
				for (int i = 0; i < 8; i++) {
					JSONObject bolsa = new JSONObject();
					switch (i) {
					case 0:
						if (lastPosMov.getAlmeria() > 0) {
							bolsa.put("lugar", "Almería");
							bolsa.put("pos", lastPosMov.getAlmeria());
							bolsa.put("change", 0);
							bolsasArray.put(bolsa);
						}
						break;
					case 1:
						if (lastPosMov.getCadiz() > 0) {
							bolsa.put("lugar", "Cádiz");
							bolsa.put("pos", lastPosMov.getCadiz());
							bolsa.put("change", 0);
							bolsasArray.put(bolsa);
						}
						break;
					case 2:
						if (lastPosMov.getCordoba() > 0) {
							bolsa.put("lugar", "Córdoba");
							bolsa.put("pos", lastPosMov.getCordoba());
							bolsa.put("change", 0);
							bolsasArray.put(bolsa);
						}
						break;
					case 3:
						if (lastPosMov.getGranada() > 0) {
							bolsa.put("lugar", "Granada");
							bolsa.put("pos", lastPosMov.getGranada());
							bolsa.put("change", 0);
							bolsasArray.put(bolsa);
						}
						break;
					case 4:
						if (lastPosMov.getHuelva() > 0) {
							bolsa.put("lugar", "Huelva");
							bolsa.put("pos", lastPosMov.getHuelva());
							bolsa.put("change", 0);
							bolsasArray.put(bolsa);
						}
						break;
					case 5:
						if (lastPosMov.getJaen() > 0) {
							bolsa.put("lugar", "Jaén");
							bolsa.put("pos", lastPosMov.getJaen());
							bolsa.put("change", 0);
							bolsasArray.put(bolsa);
						}
						break;
					case 6:
						if (lastPosMov.getMalaga() > 0) {
							bolsa.put("lugar", "Málaga");
							bolsa.put("pos", lastPosMov.getMalaga());
							bolsa.put("change", 0);
							bolsasArray.put(bolsa);
						}
						break;
					case 7:
						if (lastPosMov.getSevilla() > 0) {
							bolsa.put("lugar", "Sevilla");
							bolsa.put("pos", lastPosMov.getSevilla());
							bolsa.put("change", 0);
							bolsasArray.put(bolsa);
						}
						break;
					default:
						break;
					}

				}
				returnJSON.put("posicionglobal", lastPosMov.getGeneral());
				returnJSON.put("bolsas", bolsasArray);
				returnJSON.put("lastChangeDate",
						formato.format(lastPosMov.getFecha()));

			}

		} catch (Exception e) {
			Log.e("bolsas", "Error realizando la consulta de las bolsas -->"
					+ e.getMessage());
		} finally {
			db.close();
			// System.gc();
		}

		return returnJSON;
	}

	private String limpiarAcentos(String cadena) {

		return cadena.replaceAll("&aacute;", "a").replaceAll("&eacute;", "e")
				.replaceAll("&oacute;", "o").replaceAll("&iacute;", "i");
	}

	/**
	 * Añade los interceptores de GZIP
	 *
	 * @param httpclient
	 */
	private void addGzipInterceptors(DefaultHttpClient httpclient) {
		httpclient.addRequestInterceptor(new HttpRequestInterceptor() {

			public void process(final HttpRequest request,
					final HttpContext context) throws HttpException,
					IOException {
				if (!request.containsHeader("Accept-Encoding")) {
					request.addHeader("Accept-Encoding", "gzip");
				}
			}

		});

		httpclient.addResponseInterceptor(new HttpResponseInterceptor() {

			public void process(final HttpResponse response,
					final HttpContext context) throws HttpException,
					IOException {
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					Header ceheader = entity.getContentEncoding();
					if (ceheader != null) {
						HeaderElement[] codecs = ceheader.getElements();
						for (int i = 0; i < codecs.length; i++) {
							if (codecs[i].getName().equalsIgnoreCase("gzip")) {
								response.setEntity(new CtrlzAppsGzipDescompressingEntity(
										response.getEntity()));
								return;
							}
						}
					}
				}
			}
		});
	}
}